//
//  ElectriCityModel.swift
//  AwiMoney
//
//  Created by iOS on 16/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
struct ProviderRequest{
    public var sessionId : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!]
        }
    }
}

struct ProviderResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!

    var detailsList: [Providertails]?

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        guard let list = dictionary["details"] as? [Dictionary<String, Any>] else
        { return }
        self.detailsList = list.map { Providertails($0) }
    }
}

    struct Providertails {
        public var billerId : String!
        public var groupDescription : String!
        public var name : String!
        public var trans_type_id : String!
        public var ref_info_id : String!
        public var remarks : String!
        
    init(_ dict: Dictionary<String, Any>) {
        billerId = (dict["billerId"] as? String)
        groupDescription = (dict["groupDescription"] as? String)
        name = (dict["name"] as? String)
        trans_type_id = (dict["trans_type_id"] as? String)
        ref_info_id = (dict["ref_info_id"] as? String)
    }

}
