//
//  File.swift
//  AwiMoney
//
//  Created by iOS on 15/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation

struct LoadMoneyTransRequest{
    public var sessionId : String!
    public var startDate : String!
    public var endDate : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"startDate":startDate!,"endDate":endDate!]
        }
    }

    
}

struct LoadMoneyTransResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!

    var detailsList: [LoadTransHDetails]?

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        guard let list = dictionary["details"] as? [Dictionary<String, Any>] else
        { return }
        self.detailsList = list.map { LoadTransHDetails($0) }
    }
}

    struct LoadTransHDetails {
        public var amount : String!
        public var created : String!
        public var orderNo : String!
        public var txnStatus : String!
        public var username : String!
        
    init(_ dict: Dictionary<String, Any>) {
        amount = (dict["amount"] as? String)
        created = (dict["created"] as? String)
        orderNo = (dict["orderNo"] as? String)
        txnStatus = (dict["txnStatus"] as? String)
        username = (dict["username"] as? String)
    }

}


