//
//  SendMoneyModel.swift
//  AwiMoney
//
//  Created by iOS on 15/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
struct SendMoneyTransRequest{
    public var sessionId : String!
    public var startDate : String!
    public var endDate : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"startDate":startDate!,"endDate":endDate!]
        }
    }

    
}

struct SendMoneyTransResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!

    var detailsList: [SendTransHDetails]?

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        guard let list = dictionary["details"] as? [Dictionary<String, Any>] else
        { return }
        self.detailsList = list.map { SendTransHDetails($0) }
    }
}

    struct SendTransHDetails {
        public var amount : String!
        public var created : String!
        public var txnRefNumber : String!
        public var txnStatus : String!
        public var senderUserName : String!
        public var remarks : String!

        
        
    init(_ dict: Dictionary<String, Any>) {
        print("Date is not showing",dict)
        amount = (dict["amount"] as? String)
        created = (dict["transactionDate"] as? String)
        txnRefNumber = (dict["txnRefNumber"] as? String)
        txnStatus = (dict["txnStatus"] as? String)
        senderUserName = (dict["recieverUserName"] as? String)
        remarks = (dict["remarks"] as? String)

    }

}
