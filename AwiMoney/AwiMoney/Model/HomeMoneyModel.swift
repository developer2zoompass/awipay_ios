//
//  HomeMoneyModel.swift
//  AwiMoney
//
//  Created by iOS on 12/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation

struct LoadMoney_Request
{
    public var sessionId : String!
    public var username : String!
    public var cardExpiryDate : String!
    public var cardNumber : String!
    public var amount : String!
    public var cardType : String!
    public var cardCVV2 : String!


    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"username":username!,"cardExpiryDate":cardExpiryDate!,"cardNumber":cardNumber!,"amount":amount!,"cardType":cardType!,"cardCVV2":cardCVV2!]
        }
    }
}
struct LoadMoney_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var orderNo : String!
    public var txnStatus : String!
    public var details : Dictionary<String, Any>!
    

    init(dictionary: [String: Any]){
        print(dictionary);
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        orderNo = (dictionary["orderNo"] as? String)
        txnStatus = (dictionary["txnStatus"] as? String)
        details = (dictionary["details"] as? Dictionary<String, Any>)
    }
}
struct RequestMoney_Request
{
    public var sessionId : String!
    public var recieverUserName : String!
    public var senderUserName : String!
    public var amount : String!
    public var remarks : String!
    public var firstName : String!


    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"recieverUserName":recieverUserName!,"senderUserName":senderUserName!,"amount":amount!,"remarks":remarks!,"firstName":firstName!]
        }
    }
}
struct RequestMoney_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var orderNo : String!
    public var txnStatus : String!
    public var details : Dictionary<String, Any>!
    

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        orderNo = (dictionary["orderNo"] as? String)
        txnStatus = (dictionary["txnStatus"] as? String)
        details = (dictionary["details"] as? Dictionary<String, Any>)
    }
}

struct SendMoneyToReqUser_Request
{
    public var sessionId : String!
    public var senderUserName : String!
    public var recieverUserName : String!
    public var amount : String!
    public var remarks : String!
    public var action : String!


    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"senderUserName":senderUserName!,"recieverUserName":recieverUserName!,"amount":amount!,"remarks":remarks!,"action":action!]
        }
    }
}
struct SendMoneyToReqUser_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var transactionNumber : String!
    

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        transactionNumber = (dictionary["transactionNumber"] as? String)
    }
}

struct SendMoney_Request
{
    public var sessionId : String!
    public var senderUsername : String!
    public var recieverUsername : String!
    public var amount : String!
    public var remarks : String!


    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"senderUsername":senderUsername!,"recieverUsername":recieverUsername!,"amount":amount!,"remarks":remarks!]
        }
    }
}
struct SendMoney_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var txnRefNumber : String!
    

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        txnRefNumber = (dictionary["txnRefNumber"] as? String)
    }
}


