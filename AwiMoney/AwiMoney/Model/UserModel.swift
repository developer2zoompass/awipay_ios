//
//  UserModel.swift
//  AwiMoney
//
//  Created by iOS on 07/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation

struct Mobile_OTP_Request
{
    public var username : String!
    var description:[String:Any] {
        get {
            return ["username":username!]
        }
    }
}

struct Mobile_otp_Reponse {
    public var message : String!
    public var status : String!
    public var success : Bool!

    init(dictionary: [String: Any]){
        message = (dictionary["message"] as! String)
        status = (dictionary["status"] as! String)
        success = (dictionary["success"] as! Bool)
    }
}
struct ForgotPsw_OTP_Request
{
    public var username : String!
    var description:[String:Any] {
        get {
            return ["username":username!]
        }
    }
}

struct ForgotPsw_OTP_Response {
    public var message : String!
    public var status : String!
    public var success : Bool!

    init(dictionary: [String: Any]){
        message = (dictionary["message"] as! String)
        status = (dictionary["status"] as! String)
        success = (dictionary["success"] as! Bool)
    }
}


struct ForgotPsw_Renew_OTP_Request
{
    public var username : String!
    public var newPassword : String!
    public var confirmPassword : String!
    public var key : String!


    var description:[String:Any] {
        get {
            return ["username":username!,"newPassword":newPassword!,"confirmPassword":confirmPassword!,"key":key!]
        }
    }
}

struct ForgotPsw_Renew_OTP_Response {
    public var message : String!
    public var status : String!
    public var success : Bool!

    init(dictionary: [String: Any]){
        message = (dictionary["message"] as! String)
        status = (dictionary["status"] as! String)
        success = (dictionary["success"] as! Bool)
    }
}

struct Resend_OTP_Request
{
    public var username : String!
    var description:[String:Any] {
        get {
            return ["username":username!]
        }
    }
}

struct Resend_OTP__Response {
    public var message : String!
    public var status : String!
    public var success : Bool!

    init(dictionary: [String: Any]){
        message = (dictionary["message"] as! String)
        status = (dictionary["status"] as! String)
        success = (dictionary["success"] as! Bool)
    }
}



//


struct Verify_OTP_request
{
    public var username : String!
    public var key : String!
    var description:[String:Any] {
        get {
            return ["username":username!, "key":key!]
        }
    }
}


struct Verify_OTP_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as! Bool)
        message = (dictionary["message"] as! String)
        status = (dictionary["status"] as! String)
    }
}

struct Registation_Form_request
{
    public var firstName : String!
    public var middleName : String!
    public var lastName : String!
    public var email : String!
    public var dateOfBirth : String!
    public var contactNo : String!
    public var password : String!
    public var confirmPassword : String!
    public var referralCode : String!

    var description:[String:Any] {
        get {
            return ["firstName":firstName!, "middleName":middleName!,"lastName":lastName!, "email":email!,"dateOfBirth":dateOfBirth!, "contactNo":contactNo!,"password":password!,"confirmPassword":confirmPassword!,"referralCode":referralCode!]
        }
    }
}

struct Registation_Form_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!

    init(dictionary: [String: Any]){
        
        print(dictionary);
        success = (dictionary["success"] as! Bool)
        message = (dictionary["message"] as! String)
        status = (dictionary["status"] as! String)
    }
}

struct Login_request
{
    public var username : String!
    public var password : String!
    public var gcmId : String!

    

    var description:[String:Any] {
        get {
            return ["username":username!, "password":password!, "gcmId":gcmId!]
        }
    }
}


struct Login_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var sessionId : String!

    

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        sessionId = (dictionary["sessionId"] as? String)


    }
}
struct User_Logout
{
    public var sessionId : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!]
        }
    }
}

struct User_Logout_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var details : Dictionary<String, Any>!
    

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        details = (dictionary["details"] as? Dictionary<String, Any>)
    }
}




// User Details

struct User_details_request
{
    public var sessionId : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!]
        }
    }
}



struct User_details_Response
{
    public var success : Bool!
    public var message : String!
    public var status : String!
    public var details : Dictionary<String, Any>?
    


//    let details =  user_details_Response?.details
//    user_details = Details(details!)

    init(dictionary: Dictionary<String, Any>){
        
        print(dictionary);
        
        success = (dictionary["success"] as? Bool)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        details = (dictionary["details"] as? Dictionary<String, Any>)
    }
}









struct Details {
    public var username : String!
    public var sessionId : String!
    public var walletDetails :  Dictionary<String, Any>!
    public var userDetails :  Dictionary<String, Any>!

    
    


    init(_ dict: Dictionary<String, Any>) {
        print(dict)
        
        username = (dict["username"] as? String)
        sessionId = (dict["sessionId"] as? String)
        walletDetails = dict["walletDetails"] as? Dictionary<String, Any>
        userDetails = dict["userDetail"] as? Dictionary<String, Any>


    }

}



struct WalletDetails {
    public var id : Int!
    public var balance : Double!
    public var credit : Int!
    public var debit : Int!

    init(_ dict: Dictionary<String, Any>) {
        
        id = (dict["id"] as? Int)
        balance = (dict["balance"] as? Double)
        credit = (dict["credit"] as? Int)
        debit = (dict["debit"] as? Int)
    }

}

struct RewardMoneyWallet {
    public var id : Int!
    public var balance : Double!

    init(_ dict: Dictionary<String, Any>) {
        
        id = (dict["id"] as? Int)
        balance = (dict["balance"] as? Double)
    }

}

struct UserDetails {
    public var firstName : String!
    public var lastName : String!
    public var address : String!
    public var contactNo : String!
    public var email : String!
    public var image : String!
    public var dateOfBirth : String!
    public var gender : String!
    public var userreferralCode : String!
    public var rewardMoneyWallet :  Dictionary<String, Any>!


    init(_ dict: Dictionary<String, Any>) {
        
        print(dict);
        
        firstName = (dict["firstName"] as? String)
        lastName = (dict["lastName"] as? String)
        address = (dict["address"] as? String)
        contactNo = (dict["contactNo"] as? String)
        
        email = (dict["email"] as? String)
        image = (dict["image"] as? String)
        dateOfBirth = (dict["dateOfBirth"] as? String)
        gender = (dict["gender"] as? String)
        
        userreferralCode = (dict["userreferralCode"] as? String)
        rewardMoneyWallet = dict["rewardMoneyWallet"] as? Dictionary<String, Any>


    }

}

struct AccountDetails {
    public var id : Int!
    public var created : Int!
    public var lastModified : Int!
    public var username : String!
    public var userType : String!
    public var authority : String!
    public var emailStatus : String!
    public var mobileStatus : String!



    
    init(_ dict: Dictionary<String, Any>) {
        
        id = (dict["id"] as? Int)
        created = (dict["created"] as? Int)
        lastModified = (dict["lastModified"] as? Int)
        username = (dict["username"] as? String)
        userType = (dict["userType"] as? String)
        authority = (dict["authority"] as? String)
        emailStatus = (dict["emailStatus"] as? String)
        mobileStatus = (dict["mobileStatus"] as? String)

    }

}



