//
//  AllReqMoneyTarsHisModel.swift
//  AwiMoney
//
//  Created by iOS on 16/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
struct AllRequestMoneyTransRequest{
    public var sessionId : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!]
        }
    }

    
}

struct AllRequestMoneyTransResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!

    var detailsList: [AllRequestTransHDetails]?

    init(dictionary: [String: Any]){
    
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        guard let list = dictionary["details"] as? [Dictionary<String, Any>] else
        { return }
        self.detailsList = list.map { AllRequestTransHDetails($0) }
    }
}

    struct AllRequestTransHDetails {
        public var receiverFirstName : String!
        public var amount : String!
        public var dateAndTime : String!
        public var recieverUserName : String!
        public var referenceNumber : String!
        public var remarks : String!
        public var senderUserName : String!
        public var status : String!


        
    init(_ dict: Dictionary<String, Any>) {
        
        print(dict);
        
        receiverFirstName = (dict["receiverFirstName"] as? String)
        amount = (dict["amount"] as? String)
        dateAndTime = (dict["dateAndTime"] as? String)
        recieverUserName = (dict["recieverUserName"] as? String)
        referenceNumber = (dict["referenceNumber"] as? String)
        senderUserName = (dict["senderUserName"] as? String)
        status = (dict["status"] as? String)
        remarks = (dict["remarks"] as? String)


    }

}
