//
//  PhoneContacts.swift
//  AwiMoney
//
//  Created by iOS on 23/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
import ContactsUI


class ContactsModel  {
    var name : String
    var number : String
    var email : String
    var country : String
    var imageData : Data
    
    init(name : String,number : String,email : String,country : String,imageData : Data) {
        self.name = name
        self.number = number
        self.email = email
        self.country = country
        self.imageData = imageData
    }
    }
