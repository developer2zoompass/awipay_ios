//
//  FcmModel.swift
//  AwiMoney
//
//  Created by iOS on 29/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation

struct FCMPayload {
    var categories: [PayLoadCategory]?
    init(_  array: [Dictionary<String, Any>]) {
        self.categories = array.map { PayLoadCategory($0) }
    }
}

struct PayLoadCategory {
    var questionList: [AlterPayLoad]?
    init(_ dict: Dictionary<String, Any>) {
        guard let list = dict["alert"] as? [Dictionary<String, Any>] else
        { return }
        self.questionList = list.map { AlterPayLoad($0) }
    }
}
struct AlterPayLoad {
    let body: String?
    let title: String?
    init(_ dict: Dictionary<String, Any>) {
        print(dict);
        
        self.body = dict["body"] as? String
        self.title = dict["title"] as? String
        
    }
}




