//
//  PayBillModel.swift
//  AwiMoney
//
//  Created by iOS on 17/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
struct PayBillRequest{
    public var sessionId : String!
    public var username : String!
    public var biller_id : String!
    public var account_number : String!
    public var payment_amount : Float!
    public var trans_type_id : Int!
    public var ref_info_id : Int!
    public var ref_info_value : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"username":username!,"biller_id":biller_id!,"account_number":account_number!,"payment_amount":payment_amount!,"trans_type_id":trans_type_id!,"ref_info_id":ref_info_id!,"ref_info_value":ref_info_value!]
        }
    }
}

struct PayBillResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!
    public var details : Dictionary<String, Any>?


    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        details = (dictionary["details"] as? Dictionary<String, Any>)
    }
}

struct Bill{
    
    public var local_ref : String!
    public var transaction_date : String!
    public var bill_rate : String!
    public var account_number : String!
    public var hours_to_fulfill : String!
    public var bill_applied : String!
    public var bill_currency : String!
    public var transaction_number : String!
    public var biller_id : String!
    public var trans_type_id : String!



    init(dictionary: [String: Any]){
        local_ref = (dictionary["local_ref"] as? String)
        transaction_date = (dictionary["transaction_date"] as? String)
        bill_rate = (dictionary["bill_rate"] as? String)
        
        account_number = (dictionary["account_number"] as? String)
        hours_to_fulfill = (dictionary["hours_to_fulfill"] as? String)
        bill_applied = (dictionary["bill_applied"] as? String)
        
        bill_currency = (dictionary["bill_currency"] as? String)
        transaction_number = (dictionary["transaction_number"] as? String)
        trans_type_id = (dictionary["trans_type_id"] as? String)
        biller_id = (dictionary["biller_id"] as? String)


        
            }
}

struct Payment{
    
    public var payment_rate : String!
    public var processing_fee : String!
    public var bill_payment : String!
    public var payment_total : String!
    public var payment_currency : String!



    init(dictionary: [String: Any]){
        payment_rate = (dictionary["payment_rate"] as? String)
        processing_fee = (dictionary["processing_fee"] as? String)
        bill_payment = (dictionary["bill_payment"] as? String)
        payment_total = (dictionary["payment_total"] as? String)
        payment_currency = (dictionary["payment_currency"] as? String)

    }
}


struct transaction_fee{
    
    public var transaction_fee_amount : String!
    public var transaction_fee_rate : String!
    public var transaction_fee_currency : String!

    init(dictionary: [String: Any]){
        transaction_fee_amount = (dictionary["transaction_fee_amount"] as? String)
        transaction_fee_rate = (dictionary["transaction_fee_rate"] as? String)
        transaction_fee_currency = (dictionary["transaction_fee_currency"] as? String)

    }
}
