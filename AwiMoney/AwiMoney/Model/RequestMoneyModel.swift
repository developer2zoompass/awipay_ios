//
//  RequestMoneyModel.swift
//  AwiMoney
//
//  Created by iOS on 15/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
struct RequestMoneyTransRequest{
    public var sessionId : String!
    public var startDate : String!
    public var endDate : String!

    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"startDate":startDate!,"endDate":endDate!]
        }
    }

    
}

struct RequestMoneyTransResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!

    var detailsList: [RequestTransHDetails]?

    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        guard let list = dictionary["details"] as? [Dictionary<String, Any>] else
        { return }
        self.detailsList = list.map { RequestTransHDetails($0) }
    }
}

    struct RequestTransHDetails {
        public var amount : String!
        public var created : String!
        public var orderNo : String!
        public var txnStatus : String!
        public var senderUserName : String!

        
    init(_ dict: Dictionary<String, Any>) {
        
        print("check",dict)
        amount = (dict["amount"] as? String)
        created = (dict["dateAndTime"] as? String)
        orderNo = (dict["transacionNumber"] as? String)
        txnStatus = (dict["status"] as? String)
        senderUserName = (dict["senderUserName"] as? String)

    }

}
struct RequestMoneyAccept{
    public var sessionId : String!
    public var senderUserName : String!
    public var recieverUserName : String!
    public var amount : String!
    public var remarks : String!
    public var action : String!


    var description:[String:Any] {
        get {
            return ["sessionId":sessionId!,"senderUserName":senderUserName!,"recieverUserName":recieverUserName!,"amount":amount!,"remarks":remarks!,"action":action!]
        }
    }
}

struct RequestMoneyAcceptResponse{
    
    public var success : String!
    public var message : String!
    public var status : String!
    public var transactionNumber : String!


    init(dictionary: [String: Any]){
        success = (dictionary["success"] as? String)
        message = (dictionary["message"] as? String)
        status = (dictionary["status"] as? String)
        status = (dictionary["transactionNumber"] as? String)
    }
}



