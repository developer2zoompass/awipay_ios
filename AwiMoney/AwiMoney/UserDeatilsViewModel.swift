//
//  ApiHelper.swift
//  AwiMoney
//
//  Created by iOS on 07/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
import Alamofire

class UserInfoViewModel {
var userInfo: User_details_Response? = nil;
init() {
}

        func userDetail(params: User_details_request, success: @escaping (_ response: String?) -> Void, failure: @escaping (_ error: String?) -> Void){
            
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()
            
            let request = AF.request(URL_USER_DETAILS, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                    let res = User_details_Response(dictionary: responseValue)

                    if res.message == "User Details."{
                        self.userInfo = User_details_Response(dictionary: responseValue)
                        
                        success(res.message);


                    } else if res.message == "Please, login and try again."{
                        
                        success(res.message);

                    }


                    AppInstance.hideLoader()

                case let .failure(error):
                    print(error)
                }
            }
        }
        else{


        }
    }
            
    

}



