//
//  AppDelegate.swift
//  AwiMoney
//
//  Created by iOS on 05/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase

var AppInstance: AppDelegate!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppInstance = self
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            
        }
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        return true
        
    }
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
        
    }
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
        
    }
    // MARK: Loader Methods
    func showLoader(){
        CustomLoader.sharedInstance.startAnimation()
        
    }
    func hideLoader(){
        CustomLoader.sharedInstance.stopAnimation()
        
    }
    // MARK: FCM Notification Center
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        print("Message ID: \(messageID)")
        //      }
        
        // Print full message.
     //   print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([[.alert, .sound]])
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
            //  print(userInfo);
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        print("Message ID: \(messageID)")
        
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print full message.
        //  print(userInfo)
        completionHandler()
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        print("Message ID: \(messageID)")
        //      }
        //
        //      // Print full message.
        //      print(userInfo)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        print("Message ID: \(messageID)")
        //      }
        
        // Print full message.
        //  print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // MARK: FCM Registation Token
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       // print("Device Token =" , deviceToken)
        Messaging.messaging().apnsToken = deviceToken
        
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
       // print("Firebase registration token: \(String(describing: fcmToken))")
        let dataDict:[String: String] = ["token": fcmToken ]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
    }
    // MARK: Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL{
           // print(incomingURL)
        }
        return true
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // deeplinking
    
    //    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
    //                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
    //      let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
    //        // ...
    //      }
    //
    //      return handled
    //    }
    
    //    func handeleIncommingDynamicLink(_ dynamicLink : DynamicLink){
    //        guard let url = dynamicLink.url else {
    //            print("That's weird. My dynamic link has no url")
    //            return
    //        }
    //        print(url)
    //
    //        print("your incoming lin Parameter is", url.absoluteURL)
    //        guard let components = URLComponents(url : url, resolvingAgainstBaseURL: false),
    //              let queriItemms = components.queryItems else { return }
    //        for queryItems in queriItemms{
    //            print("params \(queryItems.name) has a value of \(queryItems.value ?? "")")
    //        }
    //        dynamicLink.matchType
    //
    //    }
    
    //// UniversalLink
    //
    //    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
    //
    //
    //        if let incomingURL = userActivity.webpageURL{
    //            print("incoming url",incomingURL)
    //            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) {
    //                (dynamicLink, error) in
    //                guard error == nil else {
    //                    print("Found an error!", error?.localizedDescription as Any)
    //                    return
    //                }
    //                if let dynamicLink = dynamicLink{
    //                    print("incoming url dynamicLink",dynamicLink)
    //
    //                    self.handeleIncommingDynamicLink(dynamicLink)
    //                }
    //            }
    //            if linkHandled{
    //                return true
    //            } else {
    //               // other things with incoming URL?
    //                return false
    //            }
    //        }
    //        return false
    //
    //        }
    //    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    //        print("I have received a URL through a custom scheme!", url.absoluteURL)
    //        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url){
    //            self.handeleIncommingDynamicLink(dynamicLink)
    //            return true
    //        } else{
    //            // handel Google or facebook sign in here
    //            return false
    //        }
    //    }
}

