//
//  PayBillViewModel.swift
//  AwiMoney
//
//  Created by iOS on 17/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
import Alamofire

class PayBillViewModel {
var billPayInfo: PayBillResponse? = nil;
init() {
}

    func payBill(paramsReq: PayBillRequest, success: @escaping (_ response: String?) -> Void, failure: @escaping (_ error: String?) -> Void){
        
      
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()
            
            let request = AF.request(URL_BILL_PAYMENT, method: .post, parameters: paramsReq.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                    let res = PayBillResponse(dictionary: responseValue)

                        self.billPayInfo = PayBillResponse(dictionary: responseValue)
                        
                        success(res.message);

                    AppInstance.hideLoader()

                case let .failure(error):
                    print(error)
                }
            }
        }
        else{


        }
    }
            
    

}
