//
//  SignUpVC.swift
//  AwiMoney
//
//  Created by iOS on 05/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class SignUpVC: UIViewController {
    
    @IBOutlet weak var tfphoneNo: DarkGreenTextField!
    var iconClick = true

    // MARK: - ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Button Actions
    @IBAction func onClickContinue(_ sender: UIButton) {
        self.validate(messgae: TF_EMPTY_FIELD)

    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    
    // MARK: - custome methods
    func clearError(){
        tfphoneNo.hasError = false
    }
    
    // MARK: -  TextFields Validations
    func validate(messgae : String)->Bool{
        if self.tfphoneNo.text == TF_EMPTY_FIELD {
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfphoneNo.hasError = true
            return false
            
        } else if self.tfphoneNo.text?.count != 10{
            let toast = SwiftToast(text: "Invalid Mobile Number ")
            self.present(toast, animated: true)
            self.tfphoneNo.hasError = true
            return false

        }
        
        
        else{
            makeAPICall()
            return true
            
        }
    }
    
    func makeAPICall(){
        let mobileno_params = Mobile_OTP_Request(username: tfphoneNo.text!)
        let request = AF.request(URL_SIGNUP_SEND_OTP, method: .post, parameters: mobileno_params.description, encoding: JSONEncoding.default).validate()
        request.responseJSON { response in
            
            switch response.result {
                
            case let .success(value):
                let response = value as! NSDictionary
                let req = Mobile_otp_Reponse(dictionary: response as! [String : Any])
                if req.message == "Mobile number already registered. Please try to login"{
                    let toast = SwiftToast(text: req.message)
                    self.present(toast, animated: true)
                    self.tfphoneNo.hasError = true
                    
                }
                    
                else if  req.message == "Your profile is not completed" {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "RegistrationFormVC") as! RegistrationFormVC
                    controller.mobileStr = self.tfphoneNo.text!
                    controller.modalPresentationStyle = .fullScreen
                    self.presentLTRVC(controller)
                    let toast = SwiftToast(text: req.message)
                    self.present(toast, animated: true)
                    self.tfphoneNo.hasError = true
                    
                }
                    
                else if  req.message == "Enter valid mobile number" {
                    let toast = SwiftToast(text: req.message)
                    self.present(toast, animated: true)
                    self.tfphoneNo.hasError = true
                    
                }
                    
                else {
                    if self.tfphoneNo.text?.count != 10{
                        let toast = SwiftToast(text: "Invalid Mobile Number ")
                        self.present(toast, animated: true)
                        self.tfphoneNo.hasError = true
                        
                    }else {
                        
                        //  Mobile is active but registration not completed
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                        controller.messageStr = req.message
                        controller.modalPresentationStyle = .fullScreen
                        self.presentLTRVC(controller)
                        let toast = SwiftToast(text: req.message)
                        self.present(toast, animated: true)
                        self.tfphoneNo.hasError = false
                        
                    }
                    
                }
                
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
