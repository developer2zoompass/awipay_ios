//
//  OtpVC.swift
//  AwiMoney
//
//  Created by iOS on 06/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class OtpVC: UIViewController {
    
    @IBOutlet weak var btnEye: UIButton!
    var messageStr = String()
    var mobileNo = String()
    var finalOTP : String = ""
    var resendInitiated:Bool = false
    var timer: Timer?
    var count = 60
    var iconClick = true


    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var tfOne: DarkGreenTextField!
    @IBOutlet weak var tfTwo: DarkGreenTextField!
    @IBOutlet weak var tfThree: DarkGreenTextField!
    @IBOutlet weak var tfFour: DarkGreenTextField!
    @IBOutlet weak var tfFive: DarkGreenTextField!
    @IBOutlet weak var tfSix: DarkGreenTextField!
    @IBOutlet weak var time_Label: UILabel!
    @IBOutlet weak var btnResednOTP: UIButton!

    // MARK: - View Life Cycles
      override func viewDidLoad() {
        super.viewDidLoad()
        let split = messageStr.split(separator: " ")
        mobileNo = String(split.suffix(1).joined(separator: [" "]))
        lblMessage.text = "An OTP has been sent to  \(mobileNo) please enter it below"
        // Do any additional setup after loading the view.
        self.startTimer()

      }
    
    // MARK: - Button Actions
    @IBAction func onClickLockEye(_ sender: UIButton) {
        
        if(iconClick == true) {
            tfOne.isSecureTextEntry = false
            tfTwo.isSecureTextEntry = false
            tfThree.isSecureTextEntry = false
            tfFour.isSecureTextEntry = false
            tfFive.isSecureTextEntry = false
            tfSix.isSecureTextEntry = false
            
            btnEye.setImage(#imageLiteral(resourceName: "ic_eye"), for: .normal)



        } else {
           tfOne.isSecureTextEntry = true
           tfTwo.isSecureTextEntry = true
           tfThree.isSecureTextEntry = true
           tfFour.isSecureTextEntry = true
           tfFive.isSecureTextEntry = true
           tfSix.isSecureTextEntry = true
            btnEye.setImage(#imageLiteral(resourceName: "ic_eyeLock"), for: .normal)

        }

        iconClick = !iconClick

    }
    @IBAction func onClickContinueBtn(_ sender: UIButton) {
        if(finalOTP.count==6){
            //Call API
            makeAPIRequest_toVerify_OTP()
            let toast = SwiftToast(text: "OTP matched.")
            self.present(toast, animated: true)

        } else{
            let toast = SwiftToast(text: "Please compelete the OTP")
            self.present(toast, animated: true)

        }
    }
    
    @IBAction func onClickResend(_ sender: UIButton) {
         self.startTimer()
         count = 60
        time_Label.isHidden = false
        makeAPICallForToREsendAPI()
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }

    
    @IBAction func onClickContinue(_ sender: Any) {
        let textObject:UITextField = sender as! UITextField
        if textObject === self.tfOne && self.tfOne.text?.count == 1{
            finalOTP = "\(self.tfOne.text!)\(self.tfTwo.text!)\(self.tfThree.text!)\(self.tfFour.text!)\(self.tfFive.text!)\(self.tfSix.text!)"
            if(finalOTP.count==6){
                self.tfSix.resignFirstResponder()
                //Call API
                makeAPIRequest_toVerify_OTP()
                
            }else{
                self.tfTwo.becomeFirstResponder()
            }
        }
        
        if textObject === self.tfTwo && self.tfTwo.text?.count == 1{
            finalOTP = "\(self.tfOne.text!)\(self.tfTwo.text!)\(self.tfThree.text!)\(self.tfFour.text!)\(self.tfFive.text!)\(self.tfSix.text!)"
            if(finalOTP.count==6){
                self.tfSix.resignFirstResponder()
                //Call API
                makeAPIRequest_toVerify_OTP()
                
            }else{
                self.tfThree.becomeFirstResponder()
            }
        }
        
        if textObject === self.tfThree && self.tfThree.text?.count == 1{
            finalOTP = "\(self.tfOne.text!)\(self.tfTwo.text!)\(self.tfThree.text!)\(self.tfFour.text!)\(self.tfFive.text!)\(self.tfSix.text!)"
            if(finalOTP.count==6){
                self.tfSix.resignFirstResponder()
                //Call API
                makeAPIRequest_toVerify_OTP()
                
            }else{
                self.tfFour.becomeFirstResponder()
            }
        }
        if textObject === self.tfFour && self.tfFour.text?.count == 1{
            finalOTP = "\(self.tfOne.text!)\(self.tfTwo.text!)\(self.tfThree.text!)\(self.tfFour.text!)\(self.tfFive.text!)\(self.tfSix.text!)"
            if(finalOTP.count==6){
                self.tfSix.resignFirstResponder()
                //Call API
                makeAPIRequest_toVerify_OTP()
                
            }else{
                self.tfFive.becomeFirstResponder()
            }
        }

        if textObject === self.tfFive && self.tfFive.text?.count == 1{
            finalOTP = "\(self.tfOne.text!)\(self.tfTwo.text!)\(self.tfThree.text!)\(self.tfFour.text!)\(self.tfFive.text!)\(self.tfSix.text!)"
            if(finalOTP.count==6){
                self.tfSix.resignFirstResponder()
                //Call API
                makeAPIRequest_toVerify_OTP()
                
            }else{
                self.tfSix.becomeFirstResponder()
            }
        }

        if textObject === self.tfSix && self.tfSix.text?.count == 1{
            finalOTP = "\(self.tfOne.text!)\(self.tfTwo.text!)\(self.tfThree.text!)\(self.tfFour.text!)\(self.tfFive.text!)\(self.tfSix.text!)"
            if(finalOTP.count==6){
                self.tfSix.resignFirstResponder()
                //Call API
                makeAPIRequest_toVerify_OTP()
                
            }else{
                let toast = SwiftToast(text: "Please compelete the OTP")
                self.present(toast, animated: true)

            }
        }
    }
    
    
    // MARK: - Custom Methods
    func startTimer() {
           if timer == nil {
               timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)
           }
       }
       
       func stopTimer() {
           if timer != nil {
               timer?.invalidate()
               timer = nil
           }
       }
       
       @objc func loop() {
           count = count-1
           time_Label.text = "\(self.minutesSeconds(seconds: count))"
        btnResednOTP.isUserInteractionEnabled = false
        btnResednOTP.titleLabel?.textColor = UIColor.gray

           if count == 0 {
               stopTimer()
            btnResednOTP.isUserInteractionEnabled = true
            btnResednOTP.titleLabel?.textColor = UIColor.white

            time_Label.isHidden = true
            self.resendInitiated = false
               
           }
       }
    func minutesSeconds (seconds : Int) -> (String) {
        let sec = (seconds % 3600) % 60
        var strSec = String(describing: sec)
        if strSec.count == 1 {
            strSec = "0" + strSec
        }
        let min = (seconds % 3600) / 60
        var strMin = String(describing: min)
        if strMin.count == 1 {
            strMin = "0" + strMin
        }
        let time = strMin + ":" + strSec//"\(min):\(sec)"
        return time
    }
    
    // MARK: - Make API Call
    func makeAPIRequest_toVerify_OTP(){
         
         let verifyOTP_params = Verify_OTP_request(username: mobileNo, key: finalOTP)
        
         let request = AF.request(URL_SIGNUP_VERIFY_OTP, method: .post, parameters: verifyOTP_params.description, encoding: JSONEncoding.default).validate()
             request.responseJSON { response in
                 switch response.result {
                 case let .success(value):
                     let res = Verify_OTP_Response(dictionary: value as! [String : Any])
                     if res.message == "OTP matched"{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)

                         let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let controller = storyboard.instantiateViewController(withIdentifier: "RegistrationFormVC") as! RegistrationFormVC
                        controller.mobileStr = self.mobileNo
                         controller.modalPresentationStyle = .fullScreen
                         self.presentLTRVC(controller)

                     } else if res.message == "The OTP you have entered is INCORRECT"{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)

                     }
                     
                     else{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)
                     }
                 case let .failure(error):
                     print(error)
                 }
             }

    }
    
    func makeAPICallForToREsendAPI(){
            if URLUtils.isNetworkAvailable(){
                AppInstance.showLoader()

             let forgotPsw_params =  Resend_OTP_Request(username: mobileNo)
            let request = AF.request(URL_RESEND_OTP, method: .post, parameters: forgotPsw_params.description, encoding: JSONEncoding.default).validate()
                request.responseJSON { response in
                    switch response.result {
                    case let .success(value):
                        
                         let res = Resend_OTP__Response(dictionary: value as! [String : Any])
                        if res.message == "Resend OTP"{
                            AppInstance.hideLoader()
                        } else{
                        }
                    case let .failure(error):
                        print(error)
                    }
                }
        }
         else{

         
            }
        }


}
