//
//  AccpetRequestMoneyVC.swift
//  AwiMoney
//
//  Created by iOS on 29/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire


class AccpetRequestMoneyVC: UIViewController {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var remarkTextField: NewTextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    var reqMoneyDict = [String: String]()
    var receiverName = String()
    var receiverContactNo = String()
    var senderContactNo = String()
    var reqAmount = String()

    // MARK: - ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
         amountLabel.text = "Request Amount: \(reqAmount)"
         lblName.text = "Receiver's Name:  \(receiverName)"
         lblMobileNo.text = "Receiver's Mobile Number: \(receiverContactNo)"

    }
    // MARK: - Button Actions
    @IBAction func onClickBack(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContainViewController") as! ContainViewController
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
      UserDefaults.standard.set(false, forKey: "isLoggedIn")

    }
    @IBAction func onClickDecline(_ sender: UIButton) {
        
    }
    @IBAction func onClickAccept(_ sender: UIButton) {
        makeAPICallForTo_Accept_decline_RequestedMoney()
    }
    // MARK: - Make API call
    func makeAPICallForTo_Accept_decline_RequestedMoney(){
        if URLUtils.isNetworkAvailable(){
            let userSessionDefaults = UserDefaults.standard
            let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
            let senderContactNo = userSessionDefaults.string(forKey: "mobileNo")!

            let params =  RequestMoneyAccept(sessionId: sessionId, senderUserName: senderContactNo, recieverUserName: receiverContactNo, amount: reqAmount, remarks: remarkTextField.text, action: "ACCEPTED")
            
            print(params);
            
            let request = AF.request(URL_ACCEPT_DECLIEN_REQUESTED_MONEY, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                print(response);
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                    let res = RequestMoneyAcceptResponse(dictionary: responseValue)
                    
                    if res.message == "Success, Transaction Successful."{
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "ContainViewController") as! ContainViewController
                        controller.modalPresentationStyle = .fullScreen
                        self.present(controller, animated: true, completion: nil)

                        
                    } else if res.message == "Please, login and try again."{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                        controller.modalPresentationStyle = .fullScreen
                        self.present(controller, animated: true, completion: nil)
                    }
                    AppInstance.hideLoader()
                    
                case let .failure(error):
                    print(error)
                }
            }
        }
        else{
            
            
        }
    }

}
