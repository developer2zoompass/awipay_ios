//
//  ForgotPswVC.swift
//  AwiMoney
//
//  Created by iOS on 06/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class ForgotPswVC: UIViewController {
    var iconClick = true

    @IBOutlet weak var eyeThree: UIButton!
    @IBOutlet weak var eyeTwo: UIButton!
    @IBOutlet weak var eyeOne: UIButton!
    @IBOutlet weak var tfOtp: NewTextField!
    @IBOutlet weak var tfPassword: NewTextField!
    @IBOutlet weak var tfConfirmPassword: NewTextField!
    var mobile = String()
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    makeAPICallForToGetForgotPasswordAPI()
        // Do any additional setup after loading the view.
    }
    // MARK: - Button Actions
    
    @IBAction func onClickLockEye(_ sender: UIButton) {
        
        print(sender.tag)
        
        if sender.tag == 0{
            if(iconClick == true) {
                tfOtp.isSecureTextEntry = false
                tfPassword.isSecureTextEntry = false
                tfConfirmPassword.isSecureTextEntry = false
                let originalImage = UIImage(named: "ic_eye-1")
                let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
                eyeOne.setImage(tintedImage, for: .normal)
                eyeOne.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor) //change color of icon

            } else {
               tfOtp.isSecureTextEntry = true
               tfPassword.isSecureTextEntry = true
               tfConfirmPassword.isSecureTextEntry = true
               // btnEye.setImage(#imageLiteral(resourceName: "ic_eyeLock"), for: .normal)
                let originalImage = UIImage(named: "ic_eyeLock-1")
                let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
                eyeOne.setImage(tintedImage, for: .normal)
                eyeOne.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor) //change color of icon

            }
            iconClick = !iconClick

        }
        
        if sender.tag == 1{
            if(iconClick == true) {
                tfOtp.isSecureTextEntry = false
                tfPassword.isSecureTextEntry = false
                tfConfirmPassword.isSecureTextEntry = false
                let originalImage = UIImage(named: "ic_eye")
                let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
                eyeTwo.setImage(tintedImage, for: .normal)
                eyeTwo.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor) //change color of icon

            } else {
               tfOtp.isSecureTextEntry = true
               tfPassword.isSecureTextEntry = true
               tfConfirmPassword.isSecureTextEntry = true
               // btnEye.setImage(#imageLiteral(resourceName: "ic_eyeLock"), for: .normal)
                let originalImage = UIImage(named: "ic_eyeLock")
                let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
                eyeTwo.setImage(tintedImage, for: .normal)
                eyeTwo.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor) //change color of icon

            }
            iconClick = !iconClick

        }

        if sender.tag == 2{
            if(iconClick == true) {
                tfOtp.isSecureTextEntry = false
                tfPassword.isSecureTextEntry = false
                tfConfirmPassword.isSecureTextEntry = false
                let originalImage = UIImage(named: "ic_eye")
                let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
                eyeThree.setImage(tintedImage, for: .normal)
                eyeThree.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor) //change color of icon

            } else {
               tfOtp.isSecureTextEntry = true
               tfPassword.isSecureTextEntry = true
               tfConfirmPassword.isSecureTextEntry = true
               // btnEye.setImage(#imageLiteral(resourceName: "ic_eyeLock"), for: .normal)
                let originalImage = UIImage(named: "ic_eyeLock")
                let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
                eyeThree.setImage(tintedImage, for: .normal)
                eyeThree.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor) //change color of icon

            }
            iconClick = !iconClick

        }
    }
    @IBAction func onClickChange(_ sender: UIButton) {
        clearError()
        makeAPICallForToREnewPassword()
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    
    // MARK: - Custom Methods

    func clearError(){
        tfPassword.hasError = false
        tfConfirmPassword.hasError = false
    }

    
    func checkField(){
        if (tfOtp.text == ""){
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
        }
         else if(tfPassword.text!.count < TF_PWD_MIN || tfPassword.text!.count > TF_PWD_MAX ){
                tfPassword.hasError = true
                tfConfirmPassword.hasError = false
            let toast = SwiftToast(text: TF_PASSWORD)
            self.present(toast, animated: true)
        }
        else if(tfPassword.text! != tfConfirmPassword.text! ){
                tfPassword.hasError = true
                tfConfirmPassword.hasError = true
            let toast = SwiftToast(text: TF_PASSWORD_MIS)
            self.present(toast, animated: true)
        }
        else {
            //Execute
            AppInstance.showLoader()
            makeAPICallForToREnewPassword()
            
        }
        
    }
    
    // MARK: - API Calls
    func makeAPICallForToGetForgotPasswordAPI(){
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()

        let forgotPsw_params =  ForgotPsw_OTP_Request(username: mobile)
        let request = AF.request(URL_FORGOT_PASSWORD_OTP, method: .post, parameters: forgotPsw_params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    
                    
                    let res = ForgotPsw_OTP_Response(dictionary: value as! [String : Any])
                    
                    AppInstance.hideLoader()

                    if res.message == "OTP sent to \(self.mobile)"{
                        AppInstance.hideLoader()
//                        let storyboard = UIStoryboard(name: "Feature", bundle: nil)
//                        let controller = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                        controller.modalPresentationStyle = .fullScreen
//                        controller.sessionId = responseValue["sessionId"] as! String
//                        self.presentLTRVC(controller)
                    } else{
                    }
                case let .failure(error):
                    print(error)
                }
            }
    }
     else{

     
        }
    }
   func makeAPICallForToREnewPassword(){
           if URLUtils.isNetworkAvailable(){
               AppInstance.showLoader()

            let forgotPsw_params =  ForgotPsw_Renew_OTP_Request(username: mobile, newPassword: tfPassword.text, confirmPassword: tfConfirmPassword.text, key: tfOtp.text)
           let request = AF.request(URL_RESEND_OTP, method: .post, parameters: forgotPsw_params.description, encoding: JSONEncoding.default).validate()
               request.responseJSON { response in
                   switch response.result {
                   case let .success(value):
                       
                   let res = ForgotPsw_Renew_OTP_Response(dictionary: value as! [String : Any])
                       if res.message == "Password Changed Successfully"{
                           AppInstance.hideLoader()
                       // Password Changed Successfull
                           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                           let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                           controller.modalPresentationStyle = .fullScreen
                           self.presentLTRVC(controller)
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)

                       } else{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)

                       }
                   case let .failure(error):
                       print(error)
                   }
               }
       }
        else{

        
           }
       }
    

}
