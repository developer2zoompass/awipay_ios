//
//  ReqMoneyTransHisNotifiTVC.swift
//  AwiMoney
//
//  Created by iOS on 16/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit

class ReqMoneyTransHisNotifiTVC: UITableViewCell {
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lbldateTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblMobileNO: UILabel!
    @IBOutlet weak var lblReason: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
