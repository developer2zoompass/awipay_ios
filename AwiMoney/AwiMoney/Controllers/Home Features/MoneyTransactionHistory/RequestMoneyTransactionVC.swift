//
//  RequestMoneyTransactionVC.swift
//  AwiMoney
//
//  Created by iOS on 15/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class RequestMoneyTransactionVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var datePickerView: CardView!
    var dateStr = String()
    var isBtnStart = false
    var trasnsHVC = String()
    var amountStr = String()


    let TRANSACTION_HISTORY_CELL = "ReqMoneyTrasnsHisTVC"
    
    var transHistory : RequestMoneyTransResponse?{
        didSet{
            print("test")
            tblView.reloadData()
        }
    }
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnEnd: WhiteBoarder!
    @IBOutlet weak var btnStart: WhiteBoarder!
    
    // MARK: - ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.string(from: datePicker.date)
        datePicker.maximumDate = Date()


    }
    // MARK: - Button Actions
    @IBAction func OnClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    
    @IBAction func onClickGo(_ sender: UIButton) {
        if (btnStart.titleLabel?.text != "Start Date") && (btnEnd.titleLabel?.text != "End date"){
            
            if (btnStart.titleLabel?.text != "") && (btnEnd.titleLabel?.text != ""){
                self.makeAPICallForTransactionsHistory()
                
            }
        }
    }
    @IBAction func onClickDone(_ sender: UIButton) {
        
        if isBtnStart {
            btnStart.setTitle(dateStr, for: .normal)

        } else {
            btnEnd.setTitle(dateStr, for: .normal)

        }
        datePickerView.isHidden = true
    }
    @IBAction func OnClickDatepicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.string(from: sender.date)

    }
    @IBAction func onClickEndDate(_ sender: WhiteBoarder) {
        datePickerView.isHidden = false
        isBtnStart = false
    }
    @IBAction func onClickStartDate(_ sender: WhiteBoarder) {
        datePickerView.isHidden = false
        isBtnStart = true

    }
    // MARK: - API Calls
    func makeAPICallForTransactionsHistory(){
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()
            let userSessionDefaults = UserDefaults.standard
                let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
            let params =  RequestMoneyTransRequest.init(sessionId: sessionId, startDate: btnStart.titleLabel?.text, endDate: btnEnd.titleLabel?.text)
            
            print(params)
            
        let request = AF.request(URL_REQUESTED_MONEY_TRANSACTION, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                print(response);
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                     let res = RequestMoneyTransResponse(dictionary: value as! [String : Any])
                    if res.message == "List of MoneyRequest"{
                        self.transHistory =  RequestMoneyTransResponse(dictionary: responseValue)
                        
                        AppInstance.hideLoader()
//                        let toast = SwiftToast(text: res.message)
//                        self.present(toast, animated: true)
                    } else{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)
                    }
                case let .failure(error):
                    print(error)
                }
            }
    }
     else{

        }
    }
    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = transHistory?.detailsList?.count else { return 0 }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: TRANSACTION_HISTORY_CELL, for: indexPath) as! ReqMoneyTrasnsHisTVC
        if let info = transHistory?.detailsList?[indexPath.row] {
            print(info)
            
            cell.lblMobileNo.text = info.senderUserName
            cell.lbldateTime.text = info.created
            cell.lblOrderNo.text = info.orderNo
            cell.lblReason.text = info.txnStatus
//            let myInt = (info.amount as NSString).integerValue
//            let doubleStr = (String(format:"%.2f",Double(myInt)))
//
//            let amount = "$\(doubleStr)USD"
//            let amount = "$\(info.amount!).00USD"
            
            let myInt = (info.amount! as NSString).floatValue
            print(myInt)
            let doubleStr = (String(format:"%.2f",Double(myInt)))
            print(doubleStr)
            let amount = "$\(doubleStr)USD"
            print(amount)

            cell.lblAmount.text = amount
            cell.lblOrderNo.text = info.orderNo
        }
        
        cell.viewBG.layer.masksToBounds = true
        cell.viewBG.layer.cornerRadius = 5
        cell.viewBG.layer.borderWidth = 2
        cell.viewBG.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.viewBG.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor).cgColor

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }

}
