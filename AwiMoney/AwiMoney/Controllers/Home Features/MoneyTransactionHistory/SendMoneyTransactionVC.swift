//
//  SendMoneyTransactionVC.swift
//  AwiMoney
//
//  Created by iOS on 15/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class SendMoneyTransactionVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var datePickerView: CardView!
    var dateStr = String()
    var isBtnStart = false
    var trasnsHVC = String()


    let TRANSACTION_HISTORY_CELL = "SendMoneyTransHisTVC"
    var transHistory : SendMoneyTransResponse?{
        didSet{
            tblView.reloadData()
        }
    }
        @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnEnd: WhiteBoarder!
    @IBOutlet weak var btnStart: WhiteBoarder!
    
    // MARK: - ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.string(from: datePicker.date)
        datePicker.maximumDate = Date()


    }
    // MARK: - Button Actions
    @IBAction func OnClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    
    @IBAction func onClickGo(_ sender: UIButton) {
        if (btnStart.titleLabel?.text != "Start Date") && (btnEnd.titleLabel?.text != "End date"){
            
            if (btnStart.titleLabel?.text != "") && (btnEnd.titleLabel?.text != ""){
                self.makeAPICallForTransactionsHistory()
                
            }
        }
    }
    @IBAction func onClickDone(_ sender: UIButton) {
        
        
        if isBtnStart {
            btnStart.setTitle(dateStr, for: .normal)

        } else {
            btnEnd.setTitle(dateStr, for: .normal)

        }
        datePickerView.isHidden = true
    }
    @IBAction func OnClickDatepicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.string(from: sender.date)

    }
    @IBAction func onClickEndDate(_ sender: WhiteBoarder) {
        datePickerView.isHidden = false
        isBtnStart = false
    }
    @IBAction func onClickStartDate(_ sender: WhiteBoarder) {
        datePickerView.isHidden = false
        isBtnStart = true

    }
    // MARK: - API Calls
    func makeAPICallForTransactionsHistory(){
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()
            let userSessionDefaults = UserDefaults.standard
                let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
            let params =  SendMoneyTransRequest.init(sessionId: sessionId, startDate: btnStart.titleLabel?.text, endDate: btnEnd.titleLabel?.text)
            
        let request = AF.request(URL_SENDMONEY_TRANSACTION, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                     let res = SendMoneyTransResponse(dictionary: value as! [String : Any])
                    if res.message == "WalletTowallet txn report"{
                        self.transHistory =  SendMoneyTransResponse(dictionary: responseValue)
                        AppInstance.hideLoader()
//                        let toast = SwiftToast(text: res.message)
//                        self.present(toast, animated: true)
                    } else{
                        AppInstance.hideLoader()
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)

                    }
                case let .failure(error):
                    print(error)
                    AppInstance.hideLoader()

                }
            }
    }
     else{

        }
    }
    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = transHistory?.detailsList?.count else { return 0 }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: TRANSACTION_HISTORY_CELL, for: indexPath) as! SendMoneyTransHisTVC
        if let info = transHistory?.detailsList?[indexPath.row] {
            
            cell.lblMobile.text = info.senderUserName
            cell.lbldateTime.text = info.created
            let myInt = (info.amount! as NSString).floatValue
            print(myInt)
            let doubleStr = (String(format:"%.2f",Double(myInt)))
            print(doubleStr)
            let amount = "$\(doubleStr)USD"
            print(amount)

            cell.lblAmount.text = amount
            cell.lblOrderNo.text = info.txnRefNumber
            cell.lblReason.text = info.remarks

        }
        
        cell.viewBG.layer.masksToBounds = true
        cell.viewBG.layer.cornerRadius = 5
        cell.viewBG.layer.borderWidth = 2
        cell.viewBG.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.viewBG.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor).cgColor

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }

}
