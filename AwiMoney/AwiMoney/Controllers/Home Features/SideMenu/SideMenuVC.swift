//
//  SideMenuVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
enum Validate_ViewController{
    
}


class SideMenuVC: UIViewController {

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userSessionDefaults = UserDefaults.standard
        let mobile = userSessionDefaults.string(forKey: "mobileNo")!
        
        mobileNo.text = "Mobile No: \(String(describing: mobile))"
        let email = userSessionDefaults.string(forKey: "emailId")!

        lblEmail.text = "Email :\(String(describing: email))"

//        lblEmail.text =  userSessionDefaults.string(forKey: "emailId")
        lblUserName.text =  userSessionDefaults.string(forKey: "userName")

    }
    // MARK: - Button Actions

    @IBAction func onClickBtn(_ sender: UIButton) {
        
    NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)

        
        let btnIndex = sender.tag

        switch btnIndex {
        case 0: break
//            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)

        case 1:
            NotificationCenter.default.post(name: NSNotification.Name("SendMoneyVC"), object: nil)

        case 2:
            NotificationCenter.default.post(name: NSNotification.Name("ReqMoneyNotificationVC"), object: nil)
            break
            
        case 3:
            NotificationCenter.default.post(name: NSNotification.Name("BillPayVC"), object: nil)

            
        case 4:
            NotificationCenter.default.post(name: NSNotification.Name("LoadMoneyVC"), object: nil)

            
        case 5:
            NotificationCenter.default.post(name: NSNotification.Name("InviteFriendsVC"), object: nil)
            
        case 6:
            NotificationCenter.default.post(name: NSNotification.Name("AwiRewardsVC"), object: nil)
            
        case 7:
            NotificationCenter.default.post(name: NSNotification.Name("SettingVC"), object: nil)

        case 8:
            
         NotificationCenter.default.post(name: NSNotification.Name("LogoutVC"), object: nil)

            
            

        case 9:
            NotificationCenter.default.post(name: NSNotification.Name("PhoneVC"), object: nil)

        case 10:
                NotificationCenter.default.post(name: NSNotification.Name("EmailVC"), object: nil)


        default:
            break
        }    }

}
