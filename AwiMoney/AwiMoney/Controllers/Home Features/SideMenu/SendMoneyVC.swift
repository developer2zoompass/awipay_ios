//
//  SendMoneyVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast
import ContactsUI
import Foundation


class SendMoneyVC: UIViewController, ContactsVCDelegate {
    @IBOutlet weak var viewPaySucess: UIView!
    @IBOutlet weak var ViewPopup: UIView!
    @IBOutlet weak var lblMobilePopup: UILabel!
    @IBOutlet weak var lblAmountUSD: UILabel!
    @IBOutlet weak var tfsendMoneyToReciverNo: WhiteTextField!
    @IBOutlet weak var tfAmount: WhiteTextField!
    @IBOutlet weak var tfMessage: WhiteTextField!
    
    // MARK: -  View Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    // MARK: - Custome Method
    func selectedContactNumber(mobileNo: String) {
        tfsendMoneyToReciverNo.text = mobileNo
        
    }
    func convertAmountToValidFormat(val:String)->String {
        let doubleVal = (val as NSString).doubleValue * 100
        let doubleStr = (String(format:"%.2f",Double(doubleVal)))
        let myInt = (doubleStr as NSString).integerValue
        let amount =  "\(myInt)"
        var finalAmount = String()
        if amount.count == 1{
            finalAmount  = "00000000000" + amount
            
        } else if amount.count == 2{
            finalAmount  = "0000000000" + amount
            
        }else if amount.count == 3{
            finalAmount  = "000000000" + amount
            
        }
        else if amount.count == 4{
            finalAmount  = "00000000" + amount
            
        }
        else if amount.count == 5{
            finalAmount  = "0000000" + amount
            
        }
        else if amount.count == 6{
            finalAmount  = "000000" + amount
            
        }
        else if amount.count == 7{
            finalAmount  = "00000" + amount
            
        }
        else if amount.count == 8{
            finalAmount  = "0000" + amount
            
        }
        else if amount.count == 9{
            finalAmount  = "000" + amount
            
        }
        return finalAmount
    }
    // MARK: -  Button Actions Tapped
    @IBAction func onClickBackPaySucess(_ sender: UIButton) {
        //        self.viewPaySucess.isHidden = true
        //        dismissRTLVC()
    }
    
    @IBAction func OnClickcontact(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        controller.modalPresentationStyle = .fullScreen
        controller.contactsVCDelegate = self
        self.presentLTRVC(controller)
        
    }
    @IBAction func onClickNo(_ sender: UIButton) {
        ViewPopup.isHidden = true
        
    }
    @IBAction func onClickYES(_ sender: UIButton) {
        makeAPICallfor_SendMoney()
        
    }
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
        
    }
    @IBAction func onClickSend(_ sender: UIButton) {
        ViewPopup.isHidden = false
        lblMobilePopup.text = "Mobile number : \(tfsendMoneyToReciverNo.text!)"
        let myInt = (tfAmount.text! as NSString).floatValue
        let doubleStr = (String(format:"%.2f",Double(myInt)))
        let amount = "$\(doubleStr)USD"
        lblAmountUSD.text = "Amount  : \(amount)"
        
    }
    @IBAction func OnclickViewTransHistory(_ sender: GreenCustomButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SendMoneyTransactionVC") as! SendMoneyTransactionVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    //MARK: - API Calls
    func makeAPICallfor_SendMoney(){
        AppInstance.showLoader()
        if URLUtils.isNetworkAvailable(){
            
            
            let userSessionDefaults = UserDefaults.standard
            let senderUserNo = userSessionDefaults.string(forKey: "mobileNo")!
            let reciverMobileNo = tfsendMoneyToReciverNo.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            let validatePhoneNumber = String((reciverMobileNo?.suffix(10))!)
//            let userSessionDefaults = UserDefaults.standard
            let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
            if senderUserNo == validatePhoneNumber {
                let toast = SwiftToast(text: "You can't Make for your self")
                self.present(toast, animated: true)
                ViewPopup.isHidden = true
                AppInstance.hideLoader()
            }
            else{
            
            let params = SendMoney_Request(sessionId:sessionId , senderUsername: senderUserNo, recieverUsername: validatePhoneNumber, amount:tfAmount.text , remarks: tfMessage.text)
            print(params);
            let request = AF.request(URL_SENDMONEY, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let res = SendMoney_Response(dictionary: value as! [String : Any])
                    if res.message == "Success, Transaction Successful."{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)
                        self.ViewPopup.isHidden = true
                        //  self.viewPaySucess.isHidden = false
                        self.dismissRTLVC()
                        
                    } else {
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)
                    }
                    AppInstance.hideLoader()
                case let .failure(error):
                    print(error)
                }
            }
        }
        } else{
            let toast = SwiftToast(text: "check your Internet conncetion")
            self.present(toast, animated: true)
            AppInstance.hideLoader()
            
        }
    }
}
extension String {
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
}
