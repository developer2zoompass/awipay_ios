//
//  AwiRewardsVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Firebase


class AwiRewardsVC: UIViewController {
    
    @IBOutlet weak var refrealWalletMoney: WhiteBoarder!
    @IBOutlet weak var btnReferalCode: GreenBoader!
    
    var refferalString = String()
    
      // MARK: ViewLife Cycles
    
        override func viewDidLoad() {
        super.viewDidLoad()
        let userSessionDefaults = UserDefaults.standard
        let refferalCode   = userSessionDefaults.string(forKey: "refferalCode")
        let rewardWalletMoney  = userSessionDefaults.string(forKey: "rewardWalletMoney")
        refrealWalletMoney.setTitle(rewardWalletMoney, for: .normal)
        btnReferalCode.setTitle(refferalCode!.uppercased(), for: .normal)
        customURL()
            
    }
    
    // MARK: Button Actions
    
    @IBAction func onClickReferral(_ sender: GreenBoader) {
        UIPasteboard.general.string = sender.titleLabel?.text
        
    }
    @IBAction func onClickBack(_ sender:  UIButton) {
        dismissRTLVC()
        
    }
    func customURL(){
        var components = URLComponents()
        components.scheme = "https"
        components.host = "www.example.com"
        components.path = "/promo"
        let promorQuery = URLQueryItem(name: "Anshu", value: refferalString)
        components.queryItems = [promorQuery]
        guard let linkparms = components.url else { return }
        print("i m sharing \(linkparms.absoluteURL)")
        //create the dymanic link
        guard let shareLink = DynamicLinkComponents.init(link: linkparms, domainURIPrefix: "https://awipromo.page.link") else {
            print("couidn't create FDL coponent")
            return
        }
        print(shareLink);
        if let myBundel = Bundle.main.bundleIdentifier{
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundel)
        }
        shareLink.iOSParameters?.appStoreID = "962194608"
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.msewa.AwiMoney")
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        shareLink.socialMetaTagParameters?.title = "xxx"
        shareLink.socialMetaTagParameters?.descriptionText = "YYY"
        guard let lonURL = shareLink.url else { return }
        print("The long dynamic link is \(lonURL.absoluteURL)")
        shareLink.shorten { [self](url, warnings , error) in
            if let error = error {
                print("oh no! got error! \(error)")
                return
            }
            if let warnings = warnings {
                for warnigs in warnings{
                    print("FDL warnins :\(warnigs)")
                }
            }
            guard let url = url else {return}
            print("I have s short url to share \(url.absoluteURL)")
            shareUrl(url: url)
            
        }
        
    }
    func shareUrl (url : URL){
        //  if let urlStr = NSURL(string: "https://awimoney.page.link") {
        let objectsToShare = [url]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popup = activityVC.popoverPresentationController {
                popup.sourceView = self.view
                popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
            }
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    
}
