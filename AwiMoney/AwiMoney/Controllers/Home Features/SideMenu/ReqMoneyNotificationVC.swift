//
//  ReqMoneyNotificationVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast


class ReqMoneyNotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let TRANSACTION_HISTORY_CELL = "ReqMoneyTransHisNotifiTVC"
    var transHistory : AllRequestMoneyTransResponse?{
        didSet{
            tblView.reloadData()
        }
    }

    @IBOutlet weak var tblView: UITableView!

    // MARK: -  View Life cycles

    override func viewDidLoad() {
        super.viewDidLoad()
        makeAPICallForTransactionsHistory()
        print("viewdidLoad repeat")
        // Do any additional setup after loading the view.
    }
    // MARK: -  Button Actions
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }


        // MARK: - API Calls
        func makeAPICallForTransactionsHistory(){
            if URLUtils.isNetworkAvailable(){
                AppInstance.showLoader()
                let userSessionDefaults = UserDefaults.standard
                    let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
                let params =  AllRequestMoneyTransRequest.init(sessionId: sessionId)
                
            let request = AF.request(URL_SEND_ALL_REQUESTED_MONEY_TRANSACTION, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
                request.responseJSON { response in
                    

                    switch response.result {
                    case let .success(value):
                        let responseValue  =  value as! Dictionary<String, Any>
                         let res = AllRequestMoneyTransResponse(dictionary: value as! [String : Any])
                        if res.message == "List of MoneyRequest"{
                            self.transHistory =  AllRequestMoneyTransResponse(dictionary: responseValue)
                            AppInstance.hideLoader()

                        } else{
                            AppInstance.hideLoader()
                            let toast = SwiftToast(text: res.message)
                            self.present(toast, animated: true)

                        }
                    case let .failure(error):
                        print(error)
                        AppInstance.hideLoader()

                    }
                }
        }
         else{

            }
        }
    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = transHistory?.detailsList?.count else { return 0 }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: TRANSACTION_HISTORY_CELL, for: indexPath) as! ReqMoneyTransHisNotifiTVC
        if let info = transHistory?.detailsList?[indexPath.row] {
            cell.lblName.text = info.receiverFirstName
            cell.lbldateTime.text = info.dateAndTime
//            let myInt = (info.amount as NSString).integerValue
//            let doubleStr = (String(format:"%.2f",Double(myInt)))
//
//            let amount = "$\(doubleStr)USD"
            let myInt = (info.amount! as NSString).floatValue
            print(myInt)
            let doubleStr = (String(format:"%.2f",Double(myInt)))
            print(doubleStr)
            let amount = "$\(doubleStr)USD"
            print(amount)

            cell.lblAmount.text = amount
            cell.lblOrderNo.text = info.referenceNumber
            cell.lblMobileNO.text = info.recieverUserName
            cell.lblReason.text = info.remarks
            cell.lblStatus.text = info.status
            
        }
        
        cell.viewBG.layer.masksToBounds = true
        cell.viewBG.layer.cornerRadius = 5
        cell.viewBG.layer.borderWidth = 2
        cell.viewBG.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.viewBG.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor).cgColor

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell : ReqMoneyTransHisNotifiTVC =  tblView.cellForRow(at: indexPath) as! ReqMoneyTransHisNotifiTVC
        
        if let info = transHistory?.detailsList?[indexPath.row] {
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AccpetRequestMoneyVC") as! AccpetRequestMoneyVC
            controller.reqAmount = info.amount
            controller.receiverName = info.receiverFirstName
            controller.receiverContactNo = info.recieverUserName
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        }

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

}
