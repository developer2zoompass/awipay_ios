//
//  SettingVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import ContactsUI

class SettingVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let contactStore = CNContactStore()
    var titleArray = [String]()
    var subTitleArray = [String]()
    var SETTING_TCELL = "SettingVCTVC"


    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleArray = ["Password","Privacy Policy","Grievance Policy","FAQ","Version"]
        self.subTitleArray = ["Change Login Password","Show Privacy Plociy","Show Grievance Policy","Show FAQ","0.0.1"]
          // self.fetchContacts()
        // Do any additional setup after loading the view.
    }
    

        @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
        
    }

    func fetchContacts(){
        var contacts = [CNContact]()
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
        let request = CNContactFetchRequest(keysToFetch: keys)

        do {
            try self.contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: SETTING_TCELL, for: indexPath) as! SettingVCTVC
        cell.lblTitle.text = titleArray[indexPath.row];
        cell.lblSubTitle.text = subTitleArray[indexPath.row];
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
