//
//  LoadMoneyVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class LoadMoneyVC: UIViewController {
    
    @IBOutlet weak var viewPaySucess: UIView!
    var loadMoneyResponse: LoadMoney_Response? {
        didSet { }
    }
    var userMobileNo = String()
    @IBOutlet weak var tfAmount: DarkGreenTextField!
    @IBOutlet weak var viewCardPopUp: CardView!
    @IBOutlet weak var tfCardNumber: WhiteTextField!
    @IBOutlet weak var tfExpdate: WhiteTextField!
    @IBOutlet weak var tfCvv: WhiteTextField!
    @IBOutlet weak var tfReEnterAmount: WhiteTextField!
    
    // MARK: - ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Button Actions
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
        
    }
    
    @IBAction func onClickBackPaySucess(_ sender: UIButton) {
        self.viewPaySucess.isHidden = true
        dismissRTLVC()
    }
    @IBAction func onClickClose(_ sender: UIButton) {
        viewCardPopUp.isHidden = true
        
    }
    
    @IBAction func OnClickProceedCard(_ sender: UIButton) {
        checkField()
        
    }
    
    @IBAction func onClickProceed(_ sender: UIButton) {
        viewCardPopUp.isHidden = false
        tfReEnterAmount.text = tfAmount.text
        
    }
    @IBAction func onClickViewTransHistory(_ sender: WhiteCustomButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoadMoneyTransactionVC") as! LoadMoneyTransactionVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    //MARK: - Custome Methods
    func convertAmountToValidFormat(val:String)->String {
        
        let doubleVal = (val as NSString).doubleValue * 100
        let doubleStr = (String(format:"%.2f",Double(doubleVal)))
        let myInt = (doubleStr as NSString).integerValue
        let amount =  "\(myInt)"
        var finalAmount = String()
        if amount.count == 1{
            finalAmount  = "00000000000" + amount
            
        } else if amount.count == 2{
            finalAmount  = "0000000000" + amount
            
        }else if amount.count == 3{
            finalAmount  = "000000000" + amount
            
        }
        else if amount.count == 4{
            finalAmount  = "00000000" + amount
            
        }
        else if amount.count == 5{
            finalAmount  = "0000000" + amount
            
        }
        else if amount.count == 6{
            finalAmount  = "000000" + amount
            
        }
        else if amount.count == 7{
            finalAmount  = "00000" + amount
            
        }
        else if amount.count == 8{
            finalAmount  = "0000" + amount
            
        }
        else if amount.count == 9{
            finalAmount  = "000" + amount
            
        }
        return finalAmount
    }
    
    //MARK: - API Calls
    func makeAPICallfor_LoadMoney(){
        
        AppInstance.showLoader()
        if URLUtils.isNetworkAvailable(){
            
            
            let userSessionDefaults = UserDefaults.standard
            let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
            let userMobileNo   = userSessionDefaults.string(forKey: "mobileNo")!

            
            let params = LoadMoney_Request(sessionId: (sessionId), username: userMobileNo, cardExpiryDate: tfExpdate.text!, cardNumber: tfCardNumber.text!, amount: convertAmountToValidFormat(val: tfAmount.text!), cardType: "visa", cardCVV2: tfCvv.text!)
            
            
            let request = AF.request(URL_LOADMONEY, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                print(response)
                
                switch response.result {
                case let .success(value):
                    let res = LoadMoney_Response(dictionary: value as! [String : Any])
                    
                    print(res.message)
                    if res.message == "Approved, Transaction is approved."{
                        self.loadMoneyResponse = LoadMoney_Response(dictionary: value as! [String : Any])
                        self.viewPaySucess.isHidden = false

                      //  let toast = SwiftToast(text: res.message)
                      //  self.present(toast, animated: true)
                       // self.dismissRTLVC()
                        
                    } else{
                        let toast = SwiftToast(text: res.message)
                        self.present(toast, animated: true)

                    }
                    
                case let .failure(error):
                    print(error)
                    
                }
                AppInstance.hideLoader()
                
            }
            
        } else{
            let toast = SwiftToast(text: "check your Internet conncetion")
            self.present(toast, animated: true)
            AppInstance.hideLoader()
            
        }
    }
    func checkField(){
        if(tfCardNumber.text!.count == 0) {
            tfCardNumber.hasError = true
            tfExpdate.hasError = false
            tfCvv.hasError = false
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
        }else if(tfExpdate.text!.count == 0) {
            tfCardNumber.hasError = false
            tfExpdate.hasError = true
            tfCvv.hasError = false
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
            
        }
        else if(tfCvv.text!.count == 0) {
            tfCardNumber.hasError = false
            tfExpdate.hasError = false
            tfCvv.hasError = true
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
        }
        else {
            //Execute
            AppInstance.showLoader()
            makeAPICallfor_LoadMoney()
        }
    }
    
}

