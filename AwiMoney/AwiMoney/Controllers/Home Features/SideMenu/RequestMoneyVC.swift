//
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

@available(iOS 13.0, *)
class RequestMoneyVC: UIViewController, ContactsVCDelegate {
    @IBOutlet weak var paySucessView: UIView!
    
    @IBOutlet weak var lblAmountPop: UILabel!
    @IBOutlet weak var lblMobilePop: UILabel!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var tfReqMoneyFromSender: WhiteTextField!
    @IBOutlet weak var tfAmount: WhiteTextField!
    @IBOutlet weak var tfMessage: WhiteTextField!
    
    // MARK: - View Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func selectedContactNumber(mobileNo: String) {
        tfReqMoneyFromSender.text = mobileNo
    }
    
    // MARK: - Button Actions
    @IBAction func onClickContacts(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        controller.modalPresentationStyle = .fullScreen
        controller.contactsVCDelegate = self
        self.presentLTRVC(controller)
        
    }
    @IBAction func onClickBackPaySucess(_ sender: UIButton) {
        //        self.paySucessView.isHidden = true
        //        self.dismissRTLVC()
        //
    }
    @IBAction func OnlcikBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    
    @IBAction func onClickYes(_ sender: UIButton) {
        makeAPICallfor_ReqMoney()
    }
    
    @IBAction func onClickNo(_ sender: UIButton) {
        viewPopup.isHidden = true
        
    }
    
    @IBAction func onClickSend(_ sender: UIButton) {
        // makeAPICallfor_ReqMoney()
        viewPopup.isHidden = false
        lblMobilePop.text = "Mobile number : \(tfReqMoneyFromSender.text!)"
        let myInt = (tfAmount.text! as NSString).floatValue
        print(myInt)
        let doubleStr = (String(format:"%.2f",Double(myInt)))
        print(doubleStr)
        let amount = "$\(doubleStr)USD"
        print(amount)
        lblAmountPop.text = "Amount  : \(amount)"
        
    }
    @IBAction func onClickViewTransHistory(_ sender: GreenCustomButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RequestMoneyTransactionVC") as! RequestMoneyTransactionVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    // MARK: - Make APICall
    
    func makeAPICallfor_ReqMoney(){
        
        AppInstance.showLoader()
        if URLUtils.isNetworkAvailable(){
            let userSessionDefaults = UserDefaults.standard
            let reqUserNo = userSessionDefaults.string(forKey: "mobileNo")!
            print(reqUserNo);
            
            if reqUserNo == tfReqMoneyFromSender.text {
                let toast = SwiftToast(text: "You can't Make for your self")
                self.present(toast, animated: true)
                viewPopup.isHidden = true
                AppInstance.hideLoader()
                
            }
            else
            {
                let userSessionDefaults = UserDefaults.standard
                let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
                let reqUserNo = userSessionDefaults.string(forKey: "mobileNo")!
                let firstName = userSessionDefaults.string(forKey: "firstName")!
                let params = RequestMoney_Request(sessionId: sessionId, recieverUserName: tfReqMoneyFromSender.text, senderUserName:reqUserNo , amount: tfAmount.text, remarks: tfMessage.text, firstName:firstName )
                let request = AF.request(URL_REQMONEY, method: .post, parameters: params.description, encoding: JSONEncoding.default).validate()
                request.responseJSON { response in
                    
                    switch response.result {
                    case let .success(value):
                        let res = RequestMoney_Response(dictionary: value as! [String : Any])
                        print(res)
                        self.viewPopup.isHidden = true
                        // self.paySucessView.isHidden = false
                        let toast = SwiftToast(text: "Success, Transaction Successful")
                        self.present(toast, animated: true)
                        self.dismissRTLVC()
                    case let .failure(error):
                        print(error)
                    }
                    AppInstance.hideLoader()
                }
                
            }
            
            
        } else{
            let toast = SwiftToast(text: "check your Internet conncetion")
            self.present(toast, animated: true)
            AppInstance.hideLoader()
            
        }
    }
}
