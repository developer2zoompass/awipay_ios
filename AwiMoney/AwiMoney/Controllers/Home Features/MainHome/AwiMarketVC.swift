//
//  AwiMarketVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class AwiMarketVC: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL (string: "https://awimarketplace.com/")
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)

    }
    
    @IBAction func OnClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
}
