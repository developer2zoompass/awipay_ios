//
//  AwiCashVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit

class AwiCashVC: UIViewController {
    var userInfoVM = UserInfoViewModel();

    @IBOutlet weak var lblBalance: UILabel!
    // MARK: - View Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        makeAPICallForToGetUserDetails()

        
    }
    var user_details_Response: User_details_Response? {
        didSet {
            let response =  user_details_Response?.details
            details = Details(response!)
        }
    }
    var details: Details? {
        didSet {

            let walletdetailRes = details?.walletDetails
            wallet = WalletDetails(walletdetailRes!)


        }
    }
    
        var wallet: WalletDetails? {
            didSet{
                
//                let sum : Int = (wallet?.balance)!
//                lblBalance.text = "$\(String(sum))USD"
                let sum : Double = (wallet?.balance)!
                let doubleStr = (String(format:"%.2f",Double(sum)))
                let amount = "$\(doubleStr)USD"
                lblBalance.text = " \(amount)"

     
            }
        }



    // MARK: - Button Actions
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    @IBAction func onClickLoad(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoadMoneyVC") as! LoadMoneyVC
        controller.modalPresentationStyle = .fullScreen
        self.presentLTRVC(controller)

    }

    @IBAction func onClickSend(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SendMoneyVC") as! SendMoneyVC
        controller.modalPresentationStyle = .fullScreen
        self.presentLTRVC(controller)

    }
    
    @IBAction func onClickRequest(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RequestMoneyVC") as! RequestMoneyVC
        controller.modalPresentationStyle = .fullScreen
        self.presentLTRVC(controller)

    }
    
    func makeAPICallForToGetUserDetails(){
       let userSessionDefaults = UserDefaults.standard
        let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!

        
        let params =  User_details_request(sessionId: sessionId)
        
        self.userInfoVM.userDetail(params: params, success: { (message) in
            
            self.user_details_Response = self.userInfoVM.userInfo
            
        }, failure: { (message) in
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//            self.present(controller, animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SplashVC")
            self.present(controller, animated: true, completion: nil)

        })
    }

}
