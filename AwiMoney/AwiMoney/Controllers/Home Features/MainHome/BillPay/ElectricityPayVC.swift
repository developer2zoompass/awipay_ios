//
//  ElectricityPayVC.swift
//  AwiMoney
//
//  Created by iOS on 10/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class ElectricityPayVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var paySucessView: UIView!
    @IBOutlet weak var viewProviderOverLay: UIView!
    @IBOutlet weak var tblView: SelfSizedTableView!
    @IBOutlet weak var tfSelectProvider: WhiteTextField!
    @IBOutlet weak var tfCustomerId: WhiteTextField!
    @IBOutlet weak var tfAmount: WhiteTextField!

    let PROVIDERCELL = "ProviderTVC"
    let paymentSucess_Identifier = "PaymentSucessVC"

    var billerId = String()
    var trasnsTypeId = String()
    var refInfoId = String()

    var providerVM = ProviderViewModel();
    var payBillVM = PayBillViewModel();

    var providerRes: ProviderResponse? {
        didSet {
        tblView.reloadData()
        }
    }
    var payBillRes: PayBillResponse? {
         didSet {
             tblView.reloadData()
         }
     }
    
    // MARK: - ViewLife Cycles
    override func
        viewDidLoad() {
        super.viewDidLoad()
        self.tblView.register(UINib.init(nibName: "ProviderTVC", bundle: nil), forCellReuseIdentifier: "ProviderTVC")
        
        tblView.maxHeight = 360
        
    }
    
    // MARK: - Button Actions
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }

    @IBAction func onClickProvider(_ sender: UIButton) {
        viewProviderOverLay.isHidden = false
        makeAPICallForToGetProvider()

    }
    
    @IBAction func onClickPay(_ sender: UIButton) {
        makeAPICallForToPayBill()
    }
    
    @IBAction func onBackPaysucess(_ sender: UIButton) {
        
        paySucessView.isHidden = true;
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContainViewController") as! ContainViewController
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)

        
    }
    @IBAction func onClickViewTransHistory(_ sender: GreenCustomButton) {
    }
    
    
    //MARK: - API Calls
    func makeAPICallForToGetProvider(){
        
            let userSessionDefaults = UserDefaults.standard
            let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
            let params = ProviderRequest(sessionId:sessionId)
            
            self.providerVM.ProviderInfo(type: "electricity", paramsReq: params,  success: { (message) in

            if message == "Billers List"{
                self.providerRes = self.providerVM.billProviderInfo
                let toast = SwiftToast(text: message)
                self.present(toast, animated: true)

            } else {
                let toast = SwiftToast(text: message)
                self.present(toast, animated: true)

            }
           
        }, failure: { (message) in
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//            self.present(controller, animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SplashVC")
            self.present(controller, animated: true, completion: nil)

        })
    }
    func makeAPICallForToPayBill(){
            let userSessionDefaults = UserDefaults.standard
             let sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
             let mobileNo = userSessionDefaults.string(forKey: "mobileNo")
                    //   - JPS  - 227394229004
            let params = PayBillRequest(sessionId:sessionId , username: mobileNo, biller_id: billerId, account_number: tfCustomerId.text, payment_amount: Float(tfAmount.text!), trans_type_id: Int(trasnsTypeId), ref_info_id: Int(refInfoId), ref_info_value: tfCustomerId.text)
        
        print(params);

            self.payBillVM.payBill(paramsReq: params,  success: { (message) in
              //  print("TEST TEST " , message)
            if message == "Bill paid successfully"{
                self.payBillRes = self.payBillVM.billPayInfo
                self.paySucessView.isHidden = false;

            //    let toast = SwiftToast(text: "Payment sucessfully Done.")
              //  self.present(toast, animated: true)
                print(self)

            } else {
                let toast = SwiftToast(text: message)
                self.present(toast, animated: true)

            }
           
        }, failure: { (message) in
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//            self.present(controller, animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SplashVC")
            self.present(controller, animated: true, completion: nil)

        })
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = providerRes?.detailsList?.count else { return 0 }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: PROVIDERCELL, for: indexPath) as! ProviderTVC
        if let info = providerRes?.detailsList?[indexPath.row] {
            cell.lblTitle?.text = info.name
        }
        cell.bgView.layer.masksToBounds = true
        cell.bgView.layer.cornerRadius = 5
        cell.bgView.layer.borderWidth = 2
        cell.bgView.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.bgView.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor).cgColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let cell = tblView.cellForRow(at: indexPath)
        if let info = providerRes?.detailsList?[indexPath.row] {
            tfSelectProvider.text = info.name
            billerId = info.billerId
            trasnsTypeId = info.trans_type_id
            refInfoId = info.ref_info_id
            viewProviderOverLay.isHidden = true
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}


