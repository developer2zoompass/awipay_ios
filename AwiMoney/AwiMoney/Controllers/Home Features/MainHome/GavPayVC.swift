//
//  GavPayVC.swift
//  AwiMoney
//
//  Created by iOS on 09/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit

class GavPayVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDataSource,UITableViewDelegate {
        let BILL_GAVPAY_VIEW_CELL_IDENTIFIER = "GavPayCVC"
        let TRANSACTION_GavTVC_CELL = "GavRecentRechargeTVC"
    

        let imagesArray = [#imageLiteral(resourceName: "ic_electricity"),#imageLiteral(resourceName: "ic_water")]
        @IBOutlet weak var cvGav: UICollectionView!
        let titleArray = ["Electricity", "Water"]

    @IBOutlet weak var imgNoRecent: UIImageView!
    @IBOutlet weak var lblNoRecentHistory: UILabel!
    @IBOutlet weak var tblGavPay: UITableView!
    override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
        }
        // MARK: - Button Actions
    @IBAction func onClickBack(_ sender: UIButton) {
        
        dismissRTLVC()
        
    }
    
        // MARK: - CollectionView delegates
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                
                return  titleArray.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = cvGav.dequeueReusableCell(withReuseIdentifier: BILL_GAVPAY_VIEW_CELL_IDENTIFIER, for: indexPath as IndexPath) as! GavPayCVC
        
                cell.imgView.image = imagesArray[indexPath.row]
                cell.lblTitle.text = titleArray[indexPath.row]
            
            
             return cell
            
        }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
             let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
             let controller = storyboard.instantiateViewController(withIdentifier: "ElectricityPayVC") as! ElectricityPayVC
             controller.modalPresentationStyle = .fullScreen
             self.presentLTRVC(controller)
            
        }
            else {
                       let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
                       let controller = storyboard.instantiateViewController(withIdentifier: "WaterPayVC") as! WaterPayVC
                       controller.modalPresentationStyle = .fullScreen
                       self.presentLTRVC(controller)
                  }

    }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            let noOfCellsInRow = 2

            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

            return CGSize(width: size, height: size)
            }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets.zero
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        
     

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }

    // MARK: - TableView delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TRANSACTION_GavTVC_CELL, for: indexPath) as! GavRecentRechargeTVC
        cell.lblType.text = "Insurnace"
        cell.lbldate.text = "22 aug 1988"
        cell.lblAmount.text = "2000"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    

}
