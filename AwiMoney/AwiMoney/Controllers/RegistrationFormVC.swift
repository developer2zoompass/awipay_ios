//
//  RegistrationFormVC.swift
//  AwiMoney
//
//  Created by iOS on 06/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast

class RegistrationFormVC: UIViewController {
    @IBOutlet weak var btn1: UIButton!
        @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var bgPickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var tfFirstName: NewTextField!
    @IBOutlet weak var tfLastName: NewTextField!
    @IBOutlet weak var tfEmail: NewTextField!
    @IBOutlet weak var tfDob: NewTextField!
    @IBOutlet weak var tfPassword: NewTextField!
    @IBOutlet weak var tfConfirmPassword: NewTextField!
    @IBOutlet weak var tfRefferalCode: NewTextField!
    var dateStr = String()
    var mobileStr = String()
    var iconClick = true

    
    // MARK: - View Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateStr = dateFormatter.string(from: datePicker.date)
            datePicker.maximumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.date
        bgPickerView.isHidden = true;
    }
    
    // MARK: - Button Actiosn
    
    
    @IBAction func onClickLockEye(_ sender: UIButton) {
        
        if(iconClick == true) {
            tfPassword.isSecureTextEntry = false
            tfConfirmPassword.isSecureTextEntry = false
           // btnEye.setImage(#imageLiteral(resourceName: "ic_eye"), for: .normal)

        } else {
           tfPassword.isSecureTextEntry = true
            tfConfirmPassword.isSecureTextEntry = true
          //  btnEye.setImage(#imageLiteral(resourceName: "ic_eyeLock"), for: .normal)
        }

        iconClick = !iconClick

    }
    @IBAction func onClickContinue(_ sender: UIButton) {
    checkField()
        

        
        
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        dismissRTLVC()
    }
    // MARK: - Date Picker

    @IBAction func OnClickDatePicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.string(from: sender.date)

    }
    
    @IBAction func onClickDone(_ sender: UIButton) {
        
        tfDob.text = dateStr
        bgPickerView.isHidden = true;
        tfDob.hasError = false

        // To resign the inputView on clicking done.

    }
    // MARK: - API Call
    func makeAPICallForSignUP(){

        let reg_Form_Params = Registation_Form_request(firstName: tfFirstName.text, middleName: "", lastName: tfLastName.text, email: tfEmail.text, dateOfBirth: tfDob.text, contactNo: mobileStr, password: tfPassword.text, confirmPassword: tfConfirmPassword.text,referralCode: tfRefferalCode.text)
        
        print("Params",reg_Form_Params);
        
        
        let request = AF.request(URL_SIGNUP, method: .post, parameters: reg_Form_Params.description, encoding: JSONEncoding.default).validate()
        request.responseJSON { response in
            AppInstance.hideLoader()
            print("Ref Res",response)
            switch response.result {
            case let .success(value):
                let req = Registation_Form_Response(dictionary:value as! [String : Any])
              //  User is already fully registered
                print("ref Mes",req.message)
                if req.message == "User Added successfully"{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                    controller.modalPresentationStyle = .fullScreen
                    self.presentLTRVC(controller)
                } else if req.message == "Please try again later."{
                    
                }
                
            case let .failure(error):
                print(error)
            }
        }
    }
    
    @IBAction func OnclickDoB(_ sender: UIButton) {
        bgPickerView.isHidden = false;
    }
    
    func clearError(){
        tfFirstName.hasError = false
        tfLastName.hasError = false
        tfEmail.hasError = false
        tfDob.hasError = false
        tfPassword.hasError = false
        tfConfirmPassword.hasError = false
    }
    
    func checkField(){
        if(tfFirstName.text!.count == 0) {
            tfFirstName.hasError = true
            tfLastName.hasError = false
                tfEmail.hasError = false
                tfDob.hasError = false
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
        }else if(tfFirstName.text!.count < 2) {
            tfFirstName.hasError = true
            tfLastName.hasError = false
                tfEmail.hasError = false
                tfDob.hasError = false
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            
            let toast = SwiftToast(text: TF_NAME)
            self.present(toast, animated: true)
            
        }else if(tfLastName.text!.count == 0) {
            tfFirstName.hasError = false
            tfLastName.hasError = true
                tfEmail.hasError = false
                tfDob.hasError = false
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
            
        }else if(tfLastName.text!.count < 2) {
            tfFirstName.hasError = false
            tfLastName.hasError = true
                tfEmail.hasError = false
                tfDob.hasError = false
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            
            let toast = SwiftToast(text: TF_NAME)
            self.present(toast, animated: true)
        }
        else if(tfEmail.text!.count == 0) {
            tfFirstName.hasError = false
            tfLastName.hasError = false
                tfEmail.hasError = true
                tfDob.hasError = false
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
        }else if(tfEmail.text!.count < 2 || !(tfEmail.text!.contains("@") && tfEmail.text!.contains("."))) {
            tfFirstName.hasError = false
            tfLastName.hasError = false
                tfEmail.hasError = true
                tfDob.hasError = false
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            let toast = SwiftToast(text: TF_EMAIL)
            self.present(toast, animated: true)

        }
        else if(tfDob.text!.count == 0) {
            tfFirstName.hasError = false
            tfLastName.hasError = false
                tfEmail.hasError = false
                tfDob.hasError = true
                tfPassword.hasError = false
                tfConfirmPassword.hasError = false
            let toast = SwiftToast(text: TF_EMPTY_FIELD)
            self.present(toast, animated: true)
        }
        else if(tfPassword.text!.count < TF_PWD_MIN || tfPassword.text!.count > TF_PWD_MAX ){
            tfFirstName.hasError = false
            tfLastName.hasError = false
                tfEmail.hasError = false
                tfDob.hasError = false
                tfPassword.hasError = true
                tfConfirmPassword.hasError = false
            let toast = SwiftToast(text: TF_PASSWORD)
            self.present(toast, animated: true)
        }
        else if(tfPassword.text! != tfConfirmPassword.text! ){
            tfFirstName.hasError = false
            tfLastName.hasError = false
                tfEmail.hasError = false
                tfDob.hasError = false
                tfPassword.hasError = true
                tfConfirmPassword.hasError = true
            let toast = SwiftToast(text: TF_PASSWORD_MIS)
            self.present(toast, animated: true)
        }
        else {
            //Execute
            AppInstance.showLoader()
            makeAPICallForSignUP()
            
        }
        
    }
}
