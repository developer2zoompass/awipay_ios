//
//  SignInVC.swift
//  AwiMoney
//
//  Created by iOS on 05/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftToast
import Firebase

var iconClick = true

class SignInVC: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var btnEye: UIButton!
    
    @IBOutlet weak var splashImg: UIImageView!
    @IBOutlet weak var tfPhoneNo: DarkGreenTextField!
    @IBOutlet weak var tfPassword: DarkGreenTextField!
    var userInfoVM = UserInfoViewModel();
    var FCM_TOKEN_ID  = String()

    var user_details_Response: User_details_Response? {
        didSet {
            let response =  user_details_Response?.details
            details = Details(response!)
        }
    }
    
    var details: Details? {
        didSet {
            let walletdetailRes = details?.walletDetails
            wallet = WalletDetails(walletdetailRes!)
            
            let userDetailsdetailRes = details?.userDetails
            userPersonalDeatils = UserDetails(userDetailsdetailRes!)
        }
    }
    var userPersonalDeatils : UserDetails? {
        
        didSet{
            let firstName =  userPersonalDeatils?.firstName
            let lastName =  userPersonalDeatils?.lastName
            let email =  userPersonalDeatils?.email
            let mobile =  userPersonalDeatils?.contactNo
            let userName = "\(firstName!)\(lastName!)"
            
            let userSessionDefaults = UserDefaults.standard
            userSessionDefaults.set(mobile, forKey: "mobileNo")
            userSessionDefaults.set(email, forKey: "emailId")
            userSessionDefaults.set(userName, forKey: "userName")
            userSessionDefaults.set(firstName, forKey: "firstName")
            
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ContainViewController") as! ContainViewController
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    var wallet: WalletDetails? {
        didSet{
            
            let sum : Double = (wallet?.balance)!
            //                lblAmount.text = "$\(String(sum))USD"
            //            lblAmount.text = "$\(String(str)).00USD"
            
        }
    }
    

    
    // MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tfPhoneNo.text = ""
        tfPassword.text = ""
        
        Messaging.messaging().token { [self] token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            self.FCM_TOKEN_ID  = "\(token)"
//            print("FCM registration tokensss: \(token)")
        
          }
        }
    }
    
    // MARK: - Button Actions
    
    @IBAction func onClickLockEye(_ sender: UIButton) {
        if(iconClick == true) {
            tfPassword.isSecureTextEntry = false
            btnEye.setImage(#imageLiteral(resourceName: "ic_eye"), for: .normal)
        } else {
            tfPassword.isSecureTextEntry = true
            btnEye.setImage(#imageLiteral(resourceName: "ic_eyeLock"), for: .normal)
        }
        
        iconClick = !iconClick
        
    }
    
    @IBAction func onClickContinue(_ sender: UIButton){
        clearError()
        makeAPICallForLogin()
    }
    
    @IBAction func onClickForgotPsw(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ForgotPswVC") as! ForgotPswVC
        controller.modalPresentationStyle = .fullScreen
        controller.mobile = tfPhoneNo.text!
        
        presentLTRVC(controller)
    }
    
    @IBAction func onClickSignup(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        controller.modalPresentationStyle = .fullScreen
        
        presentLTRVC(controller)
    }
    
    // MARK: - custome methods
    func clearError(){
        tfPassword.hasError = false
        tfPhoneNo.hasError = false
    }
    
    // MARK: -  TextFields Validations
    func validate(messgae : String)->Bool{
        if(self.tfPhoneNo.text == ""){
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = true
            tfPassword.hasError = false
            
            return false
            
        } else if(tfPassword.text!.count < TF_PWD_MIN || tfPassword.text!.count > TF_PWD_MAX ){
            tfPassword.hasError = true
            let toast = SwiftToast(text: TF_PASSWORD)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = false
            tfPassword.hasError = true
            
            return false
            
        }
            
        else if(self.tfPassword.text == ""){
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = false
            tfPassword.hasError = true
            return false
            
        }
            
        else if("Incorrect Password." == messgae){
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = false
            tfPassword.hasError = true
            return false
            
        }
            
        else if("Failed, Username not found." == messgae){
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = true
            tfPassword.hasError = false
            
            return false
            
        }
        else if("Failed, Unauthorized user." == messgae){
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = true
            tfPassword.hasError = false
            
            return false
            
        }
        else if("Please try again later." == messgae){
            let toast = SwiftToast(text: messgae)
            self.present(toast, animated: true)
            tfPhoneNo.hasError = true
            tfPassword.hasError = true
            return false
            
        }
        else {
            return true
            
        }
    }
    
    // MARK: -  TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField .resignFirstResponder()
        return true
        
    }
    
    
    
    // MARK: - API Call's
    func makeAPICallForLogin(){
        
        AppInstance.showLoader()
        if URLUtils.isNetworkAvailable(){
            
            
            print(self.FCM_TOKEN_ID);
            
            let login_params =  Login_request(username: tfPhoneNo.text, password: tfPassword.text, gcmId: FCM_TOKEN_ID)
            print("GCM Params", login_params)
            
            let request = AF.request(URL_LOGIN, method: .post, parameters: login_params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                
                switch response.result {
                    
                case let .success(value):
                    let responseValue  =  value as! NSDictionary
                    let res = Login_Response(dictionary: value as! [String : Any])
                    if res.message == "Login successful."{
                        
                        AppInstance.hideLoader()
                        let userSessionDefaults = UserDefaults.standard
                        userSessionDefaults.set(true, forKey: KEY_SESSION_VALID)
                        userSessionDefaults.set(responseValue["sessionId"], forKey: KEY_SESSION_ID)
                        
                        let userDetails_params =  User_details_request(sessionId: (responseValue["sessionId"] as! String))
                        
                        self.userInfoVM.userDetail(params: userDetails_params, success: { (message) in
                            
                            
                            self.user_details_Response = self.userInfoVM.userInfo
                            
                            
                            
                            //Success
                            //Call User Detail here
                            
                            
                            
                            
                        }, failure: { (message) in
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "SplashVC") 
                            self.present(controller, animated: true, completion: nil)
                        })
                        
                        
                        
                        //  self.makeAPICallForToGetUserDetails(sessionid: responseValue["sessionId"] as! String)
                        
                    } else{
                        if self.tfPhoneNo.text?.count == 10{
                            self.validate(messgae: res.message)
                            
                        } else{
                            let toast = SwiftToast(text: TF_EMPTY_FIELD)
                            self.present(toast, animated: true)
                            self.tfPhoneNo.hasError = true
                            
                        }
                        
                        AppInstance.hideLoader()
                    }
                    
                case let .failure(error):
                    print(error)
                    
                }
            }
            
        } else{
            let toast = SwiftToast(text: "check your Internet conncetion")
            self.present(toast, animated: true)
            AppInstance.hideLoader()
            
        }
    }

    func makeAPICallForToGetUserDetails(sessionid : String){
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()
            
            let userDetails_params =  User_details_request(sessionId: sessionid)
            
            let request = AF.request(URL_USER_DETAILS, method: .post, parameters: userDetails_params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                    let res = User_details_Response(dictionary: responseValue)
                    
                    if res.message == "User Details."{
                        self.user_details_Response = User_details_Response(dictionary: responseValue)
                        
                    } else if res.message == "Please, login and try again."{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                        controller.modalPresentationStyle = .fullScreen
                        self.presentLTRVC(controller)
                        
                        
                        
                    }
                    
                    
                    AppInstance.hideLoader()
                    
                case let .failure(error):
                    print(error)
                }
            }
        }
        else{
            
            
        }
    }
    
}
    

