//
//  HomeVC.swift
//  AwiMoney
//
//  Created by iOS on 06/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseDynamicLinks

class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var lblEmai1: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var cvHome: UICollectionView!
    @IBOutlet weak var cvPromo: BJAutoScrollingCollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var userInfoVM = UserInfoViewModel();
    let BILL_HOMECVC_VIEW_CELL_IDENTIFIER = "HomeCVC";
    let PROMOCVC_CELL_IDENTIFIER = "HomePromoCVC";
    var sessionId = String()
    var userName  = String()
    var emailId  = String()
    var mobileNo  = String()
    
    let imagesArray = [#imageLiteral(resourceName: "ic_billPay"), #imageLiteral(resourceName: "ic_awiCash"), #imageLiteral(resourceName: "ic_topUp"), #imageLiteral(resourceName: "ic_aqiMarket"), #imageLiteral(resourceName: "ic_gavPay"), #imageLiteral(resourceName: "ic_awiRewards")]
    let titleArray = ["Bill Pay","awiCash","Top Up","awiMarket","GovPay","awiRewards",]
    let promoImagesArray = [#imageLiteral(resourceName: "dummy_img"), #imageLiteral(resourceName: "dummy2_img")]
    
    var fcmPayLoad: FCMPayload? {
        didSet {
        }
    }


    var user_details_Response: User_details_Response? {
        didSet {
            let response =  user_details_Response?.details
            details = Details(response!)
        }
    }
    var details: Details? {
        didSet {
            
            let walletdetailRes = details?.walletDetails
            wallet = WalletDetails(walletdetailRes!)
            
            
            let userDetailsdetailRes = details?.userDetails
            userPersonalDeatils = UserDetails(userDetailsdetailRes!)
            
            
        }
    }
    var userPersonalDeatils : UserDetails? {
        
        didSet{
            
            let firstName =  userPersonalDeatils?.firstName
            let lastName =  userPersonalDeatils?.lastName
            let email =  userPersonalDeatils?.email
            let mobile =  userPersonalDeatils?.contactNo
            let userName = "\(firstName!)\(lastName!)"
            
            
            lblEmai1.text = email
            lblUserName.text = userName
            
            
            let rewardsWalletM = userPersonalDeatils?.rewardMoneyWallet
            
            
            if rewardsWalletM != nil{
                rewardsWallet = RewardMoneyWallet(rewardsWalletM!)
            }

            let refferalCode = userPersonalDeatils?.userreferralCode
            let userSessionDefaults = UserDefaults.standard
//            userSessionDefaults.set(rewardsWalletM, forKey: "rewardWalletMoney")
            userSessionDefaults.set(refferalCode, forKey: "refferalCode")
            
            
            userSessionDefaults.set(mobile, forKey: "mobileNo")
            userSessionDefaults.set(email, forKey: "emailId")
            userSessionDefaults.set(userName, forKey: "userName")
            userSessionDefaults.set(firstName, forKey: "firstName")
            
        }
    }
    
    var wallet: WalletDetails? {
        didSet{
            print(wallet?.balance as Any);
            let sum : Double = (wallet?.balance)!
            let doubleStr = (String(format:"%.2f",Double(sum)))
            let amount = "$\(doubleStr)USD"
            lblAmount.text = " \(amount)"
        }
    }
    
    var rewardsWallet : RewardMoneyWallet?{
        didSet{
        let sum : Double = (rewardsWallet?.balance)!
        let doubleStr = (String(format:"%.2f",Double(sum)))
        let amount = "$\(doubleStr)USD"
            
            let userSessionDefaults = UserDefaults.standard
            userSessionDefaults.set(amount, forKey: "rewardWalletMoney")
        }

    }
    

    
    // MARK: - View Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        let userSessionDefaults = UserDefaults.standard
        
        sessionId  = userSessionDefaults.string(forKey: KEY_SESSION_ID)!
        initCollectionView()
        handleSideMenu()
        
//        var isFCMPayload  = userSessionDefaults.bool(forKey: "isFCMPayload")
//
//        
//        if isFCMPayload{
//            let FCMPayload  = userSessionDefaults.string(forKey: "FCMPayload")!
//            print(FCMPayload)
//        }
//
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        makeAPICallForToGetUserDetails()
        
    }
    
    // MARK: - button Actions
    @IBAction func onClickSideMenu(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
        
    }
    
    @IBAction func OnclickLoad(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoadMoneyVC") as! LoadMoneyVC
        controller.userMobileNo = mobileNo
        controller.modalPresentationStyle = .fullScreen
        self.presentLTRVC(controller)
        
    }
    
    @IBAction func onClickSend(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SendMoneyVC") as! SendMoneyVC
        controller.modalPresentationStyle = .fullScreen
        self.presentLTRVC(controller)
        
    }
    
    @IBAction func onClickRequest(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RequestMoneyVC") as! RequestMoneyVC
        controller.modalPresentationStyle = .fullScreen
        self.presentLTRVC(controller)
        
    }
    
    @IBAction func OnClickYes(_ sender: UIButton) {
        viewLogout.isHidden = false
        makeAPICallForTo_Logout()
        
    }
    
    @IBAction func onClickNo(_ sender: UIButton) {
        viewLogout.isHidden = true
    }
    
    
    // MARK: - Handle SideMenu Bar
    func handleSideMenu(){
        let userSessionDefaults = UserDefaults.standard
        userSessionDefaults.set(mobileNo, forKey: "mobileNo")
        userSessionDefaults.set(emailId, forKey: "emailId")
        userSessionDefaults.set(userName, forKey: "userName")
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem2), name: NSNotification.Name("SendMoneyVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem3), name: NSNotification.Name("ReqMoneyNotificationVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem4), name: NSNotification.Name("BillPayVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem5), name: NSNotification.Name("LoadMoneyVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem6), name: NSNotification.Name("InviteFriendsVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem7), name: NSNotification.Name("AwiRewardsVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem8), name: NSNotification.Name("SettingVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem9), name: NSNotification.Name("LogoutVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem10), name: NSNotification.Name("PhoneVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuScreenForItem11), name: NSNotification.Name("EmailVC"), object: nil)
        
    }
    
    
    // MARK: - Handle SideMenu  Actions
    @objc func showMenuScreenForItem2() {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SendMoneyVC") as! SendMoneyVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @objc func showMenuScreenForItem3() {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReqMoneyNotificationVC") as! ReqMoneyNotificationVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    @objc func showMenuScreenForItem4() {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BillPayVC") as! BillPayVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @objc func showMenuScreenForItem5() {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoadMoneyVC") as! LoadMoneyVC
        controller.userMobileNo = mobileNo
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @objc func showMenuScreenForItem6() {
        
                if let urlStr = NSURL(string: "https://awimoney.page.link/isfirst") {
                    let objectsToShare = [urlStr]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        if let popup = activityVC.popoverPresentationController {
                            popup.sourceView = self.view
                            popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                        }
                    }
        
                    self.present(activityVC, animated: true, completion: nil)
                }

                
                
                let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "InviteFriendsVC") as! InviteFriendsVC
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)

        
//        var components = URLComponents()
//        components.scheme = "https"
//        components.host = "www.example.com"
//        components.path = "/promo"
//        let promorQuery = URLQueryItem(name: "Anshu", value: "1")
//        components.queryItems = [promorQuery]
//
//        guard let linkparms = components.url else { return }
//        print("i m sharing \(linkparms.absoluteURL)")
//
//        //create the dymanic link
//
//        guard let shareLink = DynamicLinkComponents.init(link: linkparms, domainURIPrefix: "https://awimoney.page.link") else {
//            print("couidn't create FDL coponent")
//            return
//        }
//        if let myBundel = Bundle.main.bundleIdentifier{
//            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundel)
//        }
//        shareLink.iOSParameters?.appStoreID = "962194608"
//        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.msewa.AwiMoney")
//        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
//        shareLink.socialMetaTagParameters?.title = "xxx"
//        shareLink.socialMetaTagParameters?.descriptionText = "YYY"
//        guard let lonURL = shareLink.url else { return }
//        print("The long dynamic link is \(lonURL.absoluteURL)")
//        shareLink.shorten { [self](url, warnings , error) in
//            if let error = error {
//                print("oh no! got error! \(error)")
//                return
//        }
//            if let warnings = warnings {
//                for warnigs in warnings{
//                    print("FDL warnins :\(warnigs)")
//                }
//            }
//            guard let url = url else {return}
//            print("I have s short url to share \(url.absoluteURL)")
//            shareUrl(url: url)
//
//        }
    
            
        }
        
        
        
//        if let urlStr = NSURL(string: "https://awimoney.page.link") {
//            let objectsToShare = [urlStr]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                if let popup = activityVC.popoverPresentationController {
//                    popup.sourceView = self.view
//                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
//                }
//            }
//
//            self.present(activityVC, animated: true, completion: nil)
//        }

        
        
//        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "InviteFriendsVC") as! InviteFriendsVC
//        controller.modalPresentationStyle = .fullScreen
//        self.present(controller, animated: true, completion: nil)
        
    
    @objc func showMenuScreenForItem7() {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AwiRewardsVC") as! AwiRewardsVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
    func shareUrl (url : URL){
        
              //  if let urlStr = NSURL(string: "https://awimoney.page.link") {
                    let objectsToShare = [url]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        if let popup = activityVC.popoverPresentationController {
                            popup.sourceView = self.view
                            popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                        }
                    }
        
                    self.present(activityVC, animated: true, completion: nil)
                }

    
    
    @objc func showMenuScreenForItem8() {
        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
        
    }
    @objc func showMenuScreenForItem9() {
        viewLogout.isHidden = false
        
    }
    @objc func showMenuScreenForItem10() {
        
        if let url = URL(string: "tel://\("8777668247")"),
        UIApplication.shared.canOpenURL(url) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)

//        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//        controller.modalPresentationStyle = .fullScreen
//        self.present(controller, animated: true, completion: nil)
        }
    }
    @objc func showMenuScreenForItem11() {
//        let email = "info@awipay.com"
//        if let url = URL(string: "mailto:\(email)") {
//          if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url)
//          } else {
//            UIApplication.shared.openURL(url)
//          }
//        }
        let appURL = URL(string: "mailto:info@awipay.com")!

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(appURL)
        }

        
//        let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//        controller.modalPresentationStyle = .fullScreen
//        self.present(controller, animated: true, completion: nil)
        
    }
    
    // MARK: - Custom Methods
    func initCollectionView() {
        self.cvPromo.scrollInterval = 2 //Step 2
        self.collectionViewFlowLayout.scrollDirection = .horizontal
        self.collectionViewFlowLayout.minimumInteritemSpacing = 10
        self.collectionViewFlowLayout.minimumLineSpacing = 10
        self.cvPromo.startScrolling() //Step 3
    }
    
    // MARK: - CollectionView delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvHome{
            return  titleArray.count
        }else{
            return promoImagesArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvHome{
            let cell = cvHome.dequeueReusableCell(withReuseIdentifier: BILL_HOMECVC_VIEW_CELL_IDENTIFIER, for: indexPath as IndexPath) as! HomeCVC
            cell.imgView.image = imagesArray[indexPath.row]
            cell.lblTitle.text = titleArray[indexPath.row]
            return cell
        }
        else{
            let cell = cvPromo.dequeueReusableCell(withReuseIdentifier: PROMOCVC_CELL_IDENTIFIER, for: indexPath as IndexPath) as! HomePromoCVC
            cell.imgView.image = promoImagesArray[indexPath.row]
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cvHome{
            
            let noOfCellsInRow = 3
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
            return CGSize(width: size, height: size)
            
        } else{
            return CGSize.init(width: self.cvPromo.frame.size.width, height: self.cvPromo.frame.size.height)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "BillPayVC") as! BillPayVC
            controller.modalPresentationStyle = .fullScreen
            self.presentLTRVC(controller)
            
            
        }  else if indexPath.row == 1{
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AwiCashVC") as! AwiCashVC
            controller.modalPresentationStyle = .fullScreen
            self.presentLTRVC(controller)
            
            
        } else if indexPath.row == 2{
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TopUpVC") as! TopUpVC
            controller.modalPresentationStyle = .fullScreen
            self.presentLTRVC(controller)
            
            
        } else if indexPath.row == 3{
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AwiMarketVC") as! AwiMarketVC
            controller.modalPresentationStyle = .fullScreen
            self.presentLTRVC(controller)
            
        } else if indexPath.row == 4{
            
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "GavPayVC") as! GavPayVC
            controller.modalPresentationStyle = .fullScreen
            self.presentLTRVC(controller)
            
        } else{
            
            let storyboard = UIStoryboard(name: "HomeFeature", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AwiRewardsVC") as! AwiRewardsVC
            controller.modalPresentationStyle = .fullScreen
            self.presentLTRVC(controller)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func makeAPICallForToGetUserDetails(){
        let params =  User_details_request(sessionId: self.sessionId)
        self.userInfoVM.userDetail(params: params, success: { (message) in
            if message == "User Details."{
                self.user_details_Response = self.userInfoVM.userInfo
                
            } else if message == "Please, login and try again."{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
                
            }
            
        }, failure: { (message) in
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//            self.present(controller, animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SplashVC")
            self.present(controller, animated: true, completion: nil)

        })
    }
    
    func makeAPICallForTo_Logout(){
        if URLUtils.isNetworkAvailable(){
            let userDetails_params =  User_Logout(sessionId: self.sessionId)
            
            let request = AF.request(URL_LOGOUT, method: .post, parameters: userDetails_params.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                    let res = User_Logout_Response(dictionary: responseValue)
                    
                    if res.message == "User logout successful"{
                        
                        let userSessionDefaults = UserDefaults.standard
                        userSessionDefaults.set(false, forKey: KEY_SESSION_VALID)
                        userSessionDefaults.removeObject(forKey: KEY_SESSION_ID)
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                        controller.modalPresentationStyle = .fullScreen
                        self.present(controller, animated: true, completion: nil)
                        
                    } else if res.message == "Please, login and try again."{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                        controller.modalPresentationStyle = .fullScreen
                        self.present(controller, animated: true, completion: nil)
                    }
                    AppInstance.hideLoader()
                    
                case let .failure(error):
                    print(error)
                }
            }
        }
        else{
            
            
        }
    }
    
}
