//
//  ViewController.swift
//  AwiMoney
//
//  Created by iOS on 05/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit

class ContainViewController: UIViewController {

    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var isSideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }

    
   @objc func toggleSideMenu() {
        if isSideMenuOpen {
                isSideMenuOpen = false
                sideMenuConstraint.constant = -240
        } else {
                isSideMenuOpen = true
                sideMenuConstraint.constant = 0
        }
    
            UIView.animate(withDuration: 0.3) { 
                self.view.layoutIfNeeded()
       }
    }

}

