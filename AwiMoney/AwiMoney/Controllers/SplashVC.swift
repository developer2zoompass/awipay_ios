//
//  SplashVC.swift
//  AwiMoney
//
//  Created by iOS on 05/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    let network: NetworkManager = NetworkManager.sharedInstance
    @IBOutlet weak var viewNoNet: UIView!
    

    // MARK: - ViewLife Cycles

    override func viewDidLoad() {
        super.viewDidLoad()
      
        let userSessionDefaults = UserDefaults.standard

      //  userSessionDefaults.set(true, forKey: "isFCMPayload")
        
            mainOperation(time: 4)

        

        
        
        



        
        //        checkNetwork()
        }
    
    // MARK: - Custom Methods
        func mainOperation(time : Int){
            let deadlineTime = DispatchTime.now() + .seconds(time)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
                let userSessionDefaults = UserDefaults.standard
                let sessionIsValid  = userSessionDefaults.bool(forKey: KEY_SESSION_VALID)

                if(sessionIsValid){

                          let storyboard = UIStoryboard(name: "Home", bundle: nil)
                          let controller = storyboard.instantiateViewController(withIdentifier: "ContainViewController") as! ContainViewController
                          controller.modalPresentationStyle = .fullScreen
                          self.present(controller, animated: true, completion: nil)
                        UserDefaults.standard.set(false, forKey: "isLoggedIn")

                    
                    
            }
                else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
            }
            }
            )
       }
    

        func checkNetwork(){
          NetworkManager.isReachable{ _ in
              self.viewNoNet.isHidden = true
              self.mainOperation(time: 2)
          }
          // If the network is unreachable show the offline page
          NetworkManager.isUnreachable { _ in
              self.viewNoNet.isHidden = false
              
          }
          
          network.reachability.whenReachable = { _ in
              self.viewNoNet.isHidden = true
              self.mainOperation(time: 2)
          }
          
          network.reachability.whenUnreachable = { _ in
              self.viewNoNet.isHidden = false
              print("Network,", "Unreachable");
          }
      }

}
