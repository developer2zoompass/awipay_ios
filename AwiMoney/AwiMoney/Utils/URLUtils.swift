//
//  URLUtils.swift
//  AwiMoney
//
//  Created by iOS on 07/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
import SystemConfiguration

#if DEBUG
//    let serverUrl = UserDefaults.standard.string(forKey: "backend_url")!
let serverUrl2 = "http://56.66.199.192/"
let serverUrl1 = "http://3.7.20.55/"

#else
let serverUrl2 = "http://56.66.199.192/"
let serverUrl1 = "http://3.7.20.55/"

//let serverUrl = "http://localhost:8080/"


#endif
class URLUtils {
    class func getURL(path: String) -> String{
        return serverUrl1 + "Api/v1/" + path
    }
    
    class func getURL2(path: String) -> String{
        return serverUrl2 + "Api/v1/" + path
    }
    
    class func isNetworkAvailable() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}


