//
//  ContactsVC.swift
//  AwiMoney
//
//  Created by iOS on 23/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import UIKit
import ContactsUI

protocol ContactsVCDelegate: class {
    func  selectedContactNumber(mobileNo : String)
    
}
class ContactsVC: UIViewController,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    weak var contactsVCDelegate: ContactsVCDelegate?
    var resultArray: [CNContact] = [CNContact]()
    var contactsInfo  = [CNContact]()
    var filterInfo  = [CNContact]()
    var filteredTableData = [String]()
    var searchdataAry = [String]()
    var searchdataNameArray = [String]()
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getContacts()
    }
    
    
    // MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive){
            return self.filterInfo.count
        } else{
            return self.contactsInfo.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTVC", for: indexPath as IndexPath) as! ContactsTVC
        
        if(searchActive){
            let contact = self.filterInfo[indexPath.row]
            print("theis my contact arrau \(self.filterInfo.count)")
            let formatter = CNContactFormatter()
            cell.lblName.text = formatter.string(from: contact )
            if let actualNumber = contact.phoneNumbers.first?.value {
                //Get the label of the phone number
                
                //Strip out the stuff you don't need
                print(actualNumber.stringValue)
                cell.lblPhoneNumber.text = actualNumber.stringValue
            }
        }
        
        else {
            
            let contact = self.contactsInfo[indexPath.row]
            print("theis my contact arrau \(self.contactsInfo.count)")
            let formatter = CNContactFormatter()
            cell.lblName.text = formatter.string(from: contact )
            if let actualNumber = contact.phoneNumbers.first?.value {
                //Get the label of the phone number
                //Strip out the stuff you don't need
                print(actualNumber.stringValue)
                cell.lblPhoneNumber.text = actualNumber.stringValue
            }
            else{
                cell.lblPhoneNumber.text = "N.A "
            }
            if let actualEmail = (contact as AnyObject).emailAddresses?.first?.value as String? {
                print(actualEmail)
                //        cell.PersonEmailLabel.text = actualEmail
            }
            else{
                //        cell.PersonEmailLabel.text = "N.A "
            }
            if let imageData = contact.imageData {
                //If so create the image
                let userImage = UIImage(data: imageData)
                //        cell.PersonImage.image = userImage;
            }
            
            else{
                //        cell.PersonImage.image = UIImage (named: "N.A")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : ContactsTVC  = tableView.cellForRow(at: indexPath)! as! ContactsTVC
//        let userSessionDefaults = UserDefaults.standard
//        userSessionDefaults.set(cell.lblPhoneNumber.text, forKey: "sendContcts")
//        dismiss(animated: true, completion: nil)
        if contactsVCDelegate != nil {


            contactsVCDelegate?.selectedContactNumber(mobileNo: cell.lblPhoneNumber.text!)
            dismissRTLVC()

        }


    }
    
    // MARK: Search Contacts
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterInfo = contactsInfo.filter {
            $0.givenName.range(of: searchBar.text!, options: [.caseInsensitive, .diacriticInsensitive ]) != nil ||
                $0.familyName.range(of: searchBar.text!, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
        }
        if(filterInfo.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tblView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    

    // MARK: CNContacts Methods
    func getContacts() {
        let store = CNContactStore()
        switch CNContactStore.authorizationStatus(for: .contacts){
        case .authorized:
            self.retrieveContactsWithStore(store: store)
        // This is the method we will create
        case .notDetermined:
            store.requestAccess(for: .contacts){succeeded, err in
                guard err == nil && succeeded else{
                    return
                }
                self.retrieveContactsWithStore(store: store)
            }
        default:
            print("Not handled")
        }
        
    }
    func retrieveContactsWithStore(store: CNContactStore){
        let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey,CNContactImageDataKey, CNContactEmailAddressesKey] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keysToFetch as! [CNKeyDescriptor])
        var cnContacts = [CNContact]()
        do {
            try store.enumerateContacts(with: request){
                (contact, cursor) -> Void in
                if (!contact.phoneNumbers.isEmpty) {
                }
                
                if contact.isKeyAvailable(CNContactImageDataKey) {
                    if let contactImageData = contact.imageData {
                        print(UIImage(data: contactImageData)) // Print the image set on the contact
                    }
                } else {
                    // No Image available
                    
                }
                if (!contact.emailAddresses.isEmpty) {
                }
                cnContacts.append(contact)
                self.contactsInfo = cnContacts
            }
        } catch let error {
            NSLog("Fetch contact error: \(error)")
        }
        
        NSLog(">>>> Contact list:")
        for contact in cnContacts {
            let fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? "No Name"
            NSLog("\(fullName): \(contact.phoneNumbers.description)")
        }
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
        
    }
    
    
   
    func searchContact(searchString: String) -> [CNContact] {
        //Fetch
        let contactStore: CNContactStore = CNContactStore()
        var contacts: [CNContact] = [CNContact]()
        let fetchRequest: CNContactFetchRequest = CNContactFetchRequest(keysToFetch: [CNContactVCardSerialization.descriptorForRequiredKeys()])
        
        do {
            try contactStore.enumerateContacts(with: fetchRequest, usingBlock: {
                                                contact, _ in
                                                contacts.append(contact) })
            
        } catch {
            print("Get contacts \(error)")
        }
        
        //Search
        
        for item in contacts {
            
            for email in item.emailAddresses {
                if email.value.contains(searchString) {
                    resultArray.append(item)
                }
            }
        }
        let withoutDuplicates: [CNContact] = [CNContact](Set(resultArray))
        return withoutDuplicates
        
    }
    
    
}
