//
//  Constants.swift
//  AwiMoney
//
//  Created by iOS on 07/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation


let TF_PWD_MIN = 6
let TF_PWD_MAX = 16

let TF_EMPTY_FIELD = "This field is required"
let TF_NAME = "Invalid name"
let TF_EMAIL = "Invalid email"
let TF_PASSWORD = "Invalid password, should be min 6 and max 13"
let TF_MOBILE = "Invalid mobile"
let TF_PASSWORD_MIS = "Password didn't match"


let KEY_SESSION_VALID = "prefSessionIsValid"
let KEY_SESSION_ID = "prefSessionId"


let NETWORK_FAIL_MSG  = "Network is not available now"

// MARK: - Login and Registation API's

let URL_LOGIN = URLUtils.getURL(path: "user/userlogin")
let URL_FORGOT_Renew_PASSWORD = URLUtils.getURL(path: "user/RenewPassword")
let URL_FORGOT_PASSWORD_OTP = URLUtils.getURL(path: "user/ForgotPassword/OTP")
let URL_RESEND_OTP = URLUtils.getURL(path: "user/Resend/Mobile/OTP")

 let URL_SIGNUP_SEND_OTP = URLUtils.getURL(path: "user/Register/send/OTP")
let URL_SIGNUP_VERIFY_OTP = URLUtils.getURL(path: "user/Register/Verify/OTP")
let URL_SIGNUP = URLUtils.getURL(path: "user/Register")

let URL_LOGOUT = URLUtils.getURL(path: "user/Logout")

// MARK: -  UserDetails API
let URL_USER_DETAILS = URLUtils.getURL(path: "user/userDetailsBySessionId")

// MARK: -  Load, Req , SendRequestedMoney and SedMoney

let URL_LOADMONEY = URLUtils.getURL(path: "user/fac/initiateTxn")
let URL_REQMONEY = URLUtils.getURL(path: "user/fac/requestMoney")
let URL_SENDMONEY = URLUtils.getURL(path: "user/fac/walletToWalletTxn")
//let URL_SEND_REQUESTED_MONEY = URLUtils.getURL(path: "user/fac/sendRequestedMoney")

let URL_ACCEPT_DECLIEN_REQUESTED_MONEY = URLUtils.getURL(path: "user/fac/sendRequestedMoney")



// MARK: -  View - Transactions Money

let URL_LOADMONEY_TRANSACTION = URLUtils.getURL(path: "user/fac/transactionReport")
let URL_SENDMONEY_TRANSACTION = URLUtils.getURL(path: "user/fac/walletTransactionReport")
let URL_REQUESTED_MONEY_TRANSACTION = URLUtils.getURL(path: "user/fac/allRequestMoneyReport")
let URL_SEND_ALL_REQUESTED_MONEY_TRANSACTION = URLUtils.getURL(path: "user/fac/RequestMoneyNotificationList")  //sideMenu


// MARK: -  Bill Pay


let URL_BILLPAY = URLUtils.getURL(path: "user/paymaster/payABillByAccountNumber")

// MARK: -  Provider APIS

// MARK: -  View - TOPUP

let URL_TOPUP = URLUtils.getURL(path: "user/paymaster/billers/eTopUp")

// MARK: -  View - Water

let URL_WATER_PROVIDER = URLUtils.getURL(path: "user/paymaster/billers/water")

// MARK: -  View - ELECTRICITY

let URL_ELECTRICITY_PROVIDER = URLUtils.getURL(path: "user/paymaster/billers/electricity")
let URL_ELECTRICITY_UTILITY = URLUtils.getURL(path: "ser/paymaster/billers/utility")

 
// MARK: -  View - Paymaster(Bill payment) TRANSACTIONS

let URL_BILLPAY_TRANSACTIONS_PROVIDER = URLUtils.getURL(path: "user/paymaster/transaction")
let URL_BILLER_GENERAL_CATEGORY = URLUtils.getURL(path: "user/paymaster/billers/generaln")
let URL_BILL_PAYMENT = URLUtils.getURL(path: "user/paymaster/payABillByAccountNumber")


// MARK: -  View - INSURANCE

let URL_INSURANCE_PROVIDER = URLUtils.getURL(path: "user/paymaster/billers/insurance")

// MARK: -  View - FINANCE

let URL_FINANCE_PROVIDER = URLUtils.getURL(path: "user/paymaster/billers/financial")

// MARK: -  View - SCHOOL

let URL_SCHOOL_PROVIDER = URLUtils.getURL(path: "user/paymaster/billers/school")



 
// NOT USED 13 12 11 9 8 7 5










