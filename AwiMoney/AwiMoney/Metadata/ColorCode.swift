//
//  ColorCode.swift
//  QwikerPay
//
//  Created by Kashif Imam on 22/01/20.
//  Copyright © 2020 Kashif Imam. All rights reserved.
//


import Foundation


class ColorCode {
    

    static let primaryColor = "#25A071"   
    static let primaryColorLight = "#529858"
    static let primaryDarkColor = "#25A071"
    static let accentColor = "#099256"
    
    

    //Label
    static let textDarkColor = "#444444"
    static let textLightColor = "#666666"
    static let textWhiteColor = "#FFFFFF"
    
    
    
    
    static let bgColor = "#FFFFFF"
    static let bgGrey = "#F6F6F6"
    static let navigationColor = "#FFFFFF"
    static let errorColor = "#FB1304"
    
    //Button
    static let btn_back = "#3629B7"
    static let btn_on_click = "#2e2d2d"
    static let btn_on_disable = "#BEBDBD"
    static let radioColor = "#00AEEF"
    
    
    //Button Color
    
    static let btnOne = "#8C2ce2"
    static let btnTwo = "#5a2e96"
    static let btnThree = "#4A02e0"
    
     
    static let border_color = "#FFFFFF"
    static let hint_color = "#BEBDBD"
    
    //Debit
    static let color_debit = "#089a08"
    static let color_credit = "#f61540"
    
    
    
    
    
    //TextField
    static let txtDefault = "#444444"
    static let txtError = "#B41B03"
    static let txtSuccess = "#099256"
    static let txtInActive = "#444444"
    
    
    //Custom FLoating
    
    static let txttintColor = "#3629B7"
    static let txttextColor = "#444444"
    static let txtlineColor = "#e1e1e1"
    static let txtselectedTitleColor = "#3629B7"
    static let txtselectedLineColor = "#3629B7"
    static let txtErrorColor = "#B41B03"
    
    
    //Tab
    static let tab_not_selcted = "#888888"
    static let unselectedTabColor = "#e1e1e1"
    static let selectedTabColor = "#000000"
       
    
    
}

