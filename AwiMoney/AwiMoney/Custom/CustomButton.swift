//
//  CustomButton.swift
//  QwikerPay
//
//  Created by Kashif Imam on 06/02/20.
//  Copyright © 2020 Kashif Imam. All rights reserved.
//

import UIKit


class CustomButton: UIButton {
    
    override func awakeFromNib() {
        
       self.layer.cornerRadius = 16.0
        
        self.setTitleColor(ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor), for: .normal)
        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.btn_back)
        
  }
    
    override open var isEnabled: Bool {
        didSet {
            self.backgroundColor = isEnabled ? ColorConverter.hexStringToUIColor(hex: ColorCode.btn_back) : ColorConverter.hexStringToUIColor(hex: ColorCode.btn_on_disable)
        }
    }


    
}

class GreenCustomButton: UIButton {
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 5
        self.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor).cgColor
        self.layer.borderWidth = 1
        self.setTitleColor(ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor), for: .normal)
        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor)
        
  }
    
    override open var isEnabled: Bool {
        didSet {
            self.backgroundColor = isEnabled ? ColorConverter.hexStringToUIColor(hex: ColorCode.btn_back) : ColorConverter.hexStringToUIColor(hex: ColorCode.btn_on_disable)
        }
    }
}

    class WhiteCustomButton: UIButton {
      
      override func awakeFromNib() {
          
          self.layer.cornerRadius = 5
          self.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor).cgColor
          self.layer.borderWidth = 1
          self.setTitleColor(ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor), for: .normal)
          self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor)
          
    }
      
      override open var isEnabled: Bool {
          didSet {
              self.backgroundColor = isEnabled ? ColorConverter.hexStringToUIColor(hex: ColorCode.btn_back) : ColorConverter.hexStringToUIColor(hex: ColorCode.btn_on_disable)
          }
      }


    
}

    class GreenBoader: UIButton {
      
      override func awakeFromNib() {
          
          self.layer.cornerRadius = 5
          self.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor).cgColor
          self.layer.borderWidth = 1
          
    }
      
      override open var isEnabled: Bool {
          didSet {
              self.backgroundColor = isEnabled ? ColorConverter.hexStringToUIColor(hex: ColorCode.btn_back) : ColorConverter.hexStringToUIColor(hex: ColorCode.btn_on_disable)
          }
      }
        
}

        class WhiteBoarder: UIButton {
          
          override func awakeFromNib() {
              
              self.layer.cornerRadius = 5
              self.layer.borderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor).cgColor
              self.layer.borderWidth = 1
              
        }
          
          override open var isEnabled: Bool {
              didSet {
                  self.backgroundColor = isEnabled ? ColorConverter.hexStringToUIColor(hex: ColorCode.btn_back) : ColorConverter.hexStringToUIColor(hex: ColorCode.btn_on_disable)
              }
          }


    
}

