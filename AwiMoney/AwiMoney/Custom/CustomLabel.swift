//
//  CustomLabel.swift
//  QwikerPay
//
//  Created by Kashif Imam on 22/01/20.
//  Copyright © 2020 Kashif Imam. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomLblRegular: UILabel {
    override func awakeFromNib() {
        font = UIFont(name: "HelveticaNeueRegular.ttf", size: 16.0)
    }
}

@IBDesignable
class CustomLblBold: UILabel {
    override func awakeFromNib() {
        font = UIFont(name: "HelveticaNeuBold.ttf", size: 16.0)
    }
}



