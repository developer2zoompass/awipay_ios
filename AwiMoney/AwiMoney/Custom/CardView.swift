//
//  CardView.swift
//  QwikerPay
//
//  Created by Kashif Imam on 24/01/20.
//  Copyright © 2020 Kashif Imam. All rights reserved.
//

import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor = (ColorConverter.hexStringToUIColor(hex: ColorCode.textDarkColor)).cgColor
    @IBInspectable var shadowOpacity: Float = 0.5
    
        
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        
        
        
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let img = UIImage(named: "bg_on_click_view")
//        self.backgroundColor = UIColor(patternImage: img!)
//        
//        
//    }
//    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.backgroundColor = .white
//    }
}



@IBDesignable
class CardImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
        
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        
    }
    
}


@IBDesignable
class CardTableView: UITableView {
    
    @IBInspectable var cornerRadius: CGFloat = 26
        
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
    }
    
}

