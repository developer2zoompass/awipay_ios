//
//  CustomTextField.swift
//  SparkCarNepal
//
//  Created by Kashif Imam on 06/04/19.
//  Copyright © 2019 Kashif Imam. All rights reserved.
//

import Foundation
import UIKit
import SwiftToast

class WhiteTextField: UITextField{
    override func awakeFromNib() {
        self.setCorner(radius: 10)
        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor)
        self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor))
        self.placeHolderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textDarkColor)
    }
    

    @IBInspectable var hasError: Bool = false {
        didSet {
            
            if (hasError) {
                self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.errorColor))
            } else {
                
                self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.textDarkColor))
                
            }
            
        }
    }
    
}

class DarkGreenTextField: UITextField{
    override func awakeFromNib() {
        self.setCorner(radius: 10)
        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor)
        self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor))
        self.placeHolderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor)
    }

    @IBInspectable var hasError: Bool = false {
        didSet {
            
            if (hasError) {
                self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.errorColor))
            } else {
                
                self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor))
                
            }
            
        }
    }
    
}

class DarkGreenTextField_redBorder: UITextField{
    override func awakeFromNib() {
        self.setCorner(radius: 20)
        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.primaryDarkColor)
        self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.errorColor))
        self.placeHolderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor)
    }

    @IBInspectable var hasError: Bool = false {
        didSet {
            
            if (hasError) {
                self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.errorColor))
            } else {
                
                self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor))
                
            }
            
        }
    }
    
}


class NewTextField: UITextField{
    override func awakeFromNib() {
        self.setCorner(radius: 10)
        self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.hint_color))
        self.placeHolderColor = ColorConverter.hexStringToUIColor(hex: ColorCode.hint_color)
        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textWhiteColor)
        self.textColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textDarkColor)
        
    }
    
    
    @IBInspectable var hasError: Bool = false {
        didSet {
            
            if (hasError) {
                   self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.errorColor))
            } else {
                
                   self.setBorder(width: 1, color: ColorConverter.hexStringToUIColor(hex: ColorCode.hint_color))
                
            }
            
        }
    }
    
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        textRect.origin.y -= downPadding
        textRect.origin.x -= rightPadding
        
        
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    @IBInspectable var downPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = ColorConverter.hexStringToUIColor(hex: ColorCode.textLightColor) {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.contentMode = .scaleAspectFit
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
    
}




//
//class CustomTF: SkyFloatingLabelTextField {
//
//    override func awakeFromNib() {
//        tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.txttintColor)
//        textColor = ColorConverter.hexStringToUIColor(hex: ColorCode.txttextColor)
//        lineColor = ColorConverter.hexStringToUIColor(hex: ColorCode.txtlineColor)
//
//        selectedTitleColor = ColorConverter.hexStringToUIColor(hex: ColorCode.txtselectedTitleColor)
//        selectedLineColor = ColorConverter.hexStringToUIColor(hex: ColorCode.txtselectedLineColor)
//        errorColor = ColorConverter.hexStringToUIColor(hex: ColorCode.txtErrorColor)
//
//
//
//        lineHeight = 1.0 // bottom line height in points
//        selectedLineHeight = 1.0
//    }
    
    



