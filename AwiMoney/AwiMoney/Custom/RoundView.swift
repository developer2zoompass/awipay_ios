//
//  RoundView.swift
//  QwikerPay
//
//  Created by Kashif Imam on 06/02/20.
//  Copyright © 2020 Kashif Imam. All rights reserved.
//

import UIKit


class RoundView: UIView {
    
    override func awakeFromNib() {
        
      
        
        let radius = self.frame.height / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        
        
//        let gradientLayer1 = CAGradientLayer()
//        gradientLayer1.colors = [ColorConverter.hexStringToUIColor(hex: ColorCode.btnOne).cgColor,ColorConverter.hexStringToUIColor(hex: ColorCode.btnThree).cgColor]
//        gradientLayer1.startPoint = CGPoint(x: 0, y: 0)
//        gradientLayer1.endPoint = CGPoint(x: 1.5, y: 1.5)
//        gradientLayer1.frame.size.height = self.bounds.height
//        gradientLayer1.frame.size.width = self.bounds.width
//        self.layer.insertSublayer(gradientLayer1, at: 0)
    }
    
    
}
