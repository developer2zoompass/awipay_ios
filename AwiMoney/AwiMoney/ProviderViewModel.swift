//
//  PayBillViewModel.swift
//  AwiMoney
//
//  Created by iOS on 17/10/20.
//  Copyright © 2020 Msewa. All rights reserved.
//

import Foundation
import Alamofire
class ProviderViewModel {
var billProviderInfo: ProviderResponse? = nil;
init() {
}

    func ProviderInfo(type : String, paramsReq: ProviderRequest, success: @escaping (_ response: String?) -> Void, failure: @escaping (_ error: String?) -> Void){
        
      
        if URLUtils.isNetworkAvailable(){
            AppInstance.showLoader()
            var URL = String()
            if type == "electricity"{
                 URL = URL_ELECTRICITY_PROVIDER
                
            } else if type == "insurance"{
                 URL = URL_INSURANCE_PROVIDER

            } else if type == "school"{
                URL = URL_SCHOOL_PROVIDER
                
            } else if type == "finance"{
                 URL = URL_FINANCE_PROVIDER
            }else if type == "topup"{
                 URL = URL_TOPUP
            }
            
            else {
                URL = URL_WATER_PROVIDER

            }
            
            let request = AF.request(URL, method: .post, parameters: paramsReq.description, encoding: JSONEncoding.default).validate()
            request.responseJSON { response in
                switch response.result {
                case let .success(value):
                    let responseValue  =  value as! Dictionary<String, Any>
                    let res = ProviderResponse(dictionary: responseValue)
                        self.billProviderInfo = ProviderResponse(dictionary: responseValue)
                    AppInstance.hideLoader()
                    success(res.message)

                case let .failure(error):
                    print(error)
                }
            }
        }
        else{


        }
    }
            
    

}
